/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_process.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:21:27 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 10:27:53 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static int	ft_process_2(char *input, int *id, int *len, t_listd **hd)
{
	if (strcmp(get_keystrock()->del, input) == 0 && *id < *len)
	{
		get_cursor()->index_hist = -1;
		*len = *len - 1;
		ft_listd_remove_at(hd, *id + 1);
		ft_setterm("dc");
	}
	else
		return (ft_key(input, id, len));
	return (0);
}

static int	ft_process_c(char *s, int *id, int *l, t_listd **hd)
{
	get_cursor()->index_hist = -1;
	if (*s == '\n')
		return (-1);
	if (*s == 127 && *id > 0)
	{
		*id = *id - 1;
		*l = *l - 1;
		ft_listd_remove_at(hd, *id + 1);
		ft_move_cursor(LEFT);
	}
	else if (*s >= ' ' && *s != 127)
	{
		*id = *id + 1;
		*l = *l + 1;
		ft_move_cursor(RIGHT);
		if (ft_listd_add_at(hd, *id - 1, *s))
			exit(EXIT_FAILURE);
	}
	else
		ft_setterm("bl");
	return (0);
}

int			ft_process(char *s, int rt, int *id, int *l)
{
	t_listd **head;

	head = get_listd();
	if (rt == 1)
		return (ft_process_c(s, id, l, head));
	else
		return (ft_process_2(s, id, l, head));
	return (0);
}
