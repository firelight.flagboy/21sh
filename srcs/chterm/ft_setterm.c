/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setterm.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:26:50 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/17 11:27:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

void	ft_setterm(char *term)
{
	char	*res;

	res = tgetstr(term, NULL);
	if (res)
		tputs(res, 0, ft_putchar_ent);
}
