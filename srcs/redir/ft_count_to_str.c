/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_to_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 13:00:25 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/09 13:00:31 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"

char	*ft_count_to_str(uint count)
{
	static char	s[12];
	int			len;
	uint		sa;

	sa = count;
	len = 0;
	while (count)
	{
		len++;
		count /= 8;
	}
	if (len == 0)
		len = 1;
	s[len--] = 0;
	while (len >= 0)
	{
		s[len] = (sa % 8) + '0';
		sa /= 8;
		len--;
	}
	return (s);
}
