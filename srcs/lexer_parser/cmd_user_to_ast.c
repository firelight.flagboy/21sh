/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_user_to_ast.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 14:44:04 by eduriez           #+#    #+#             */
/*   Updated: 2018/03/06 11:23:12 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer_parser.h"
#include "libft.h"

t_ast		*ft_get_ast(const char *cmd_user)
{
	t_token_list	*list_tokens;
	t_ast			*ast;

	ast = NULL;
	list_tokens = ft_lex_cmd(cmd_user);
	if (list_tokens == NULL)
		return (NULL);
	if (!ft_is_valid_cmd(list_tokens))
	{
		ft_delete_list(list_tokens);
		return (NULL);
	}
	if (!ft_to_ast(list_tokens, NULL, &ast))
	{
		ft_delete_list(list_tokens);
		ft_delete_ast(ast);
		return (NULL);
	}
	ft_delete_list(list_tokens);
	return (ast);
}
