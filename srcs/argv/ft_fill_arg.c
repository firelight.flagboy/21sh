/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill_arg.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 14:57:55 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 11:10:23 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

static void	ft_fill_env(char *d, char **s, int *i)
{
	char	*e;
	char	env[2048];
	int		p;

	e = *s;
	p = 1;
	if (*e == '~')
		ft_strcpy(env, "HOME");
	else
	{
		p = ft_get_env_name(env, e);
		env[p - 1] = 0;
	}
	*s = e + p;
	if (!(e = ft_getenv(get_env(), env)))
		return ;
	p = 0;
	while (e[p])
	{
		d[*i + p] = e[p];
		p++;
	}
	d[*i + p] = 0;
	*i = *i + p;
}

void		ft_fill_arg(char *d, char *s)
{
	char	type;
	int		i;
	char	*t;

	type = 0;
	i = 0;
	t = 0;
	while (*s)
	{
		if (type == 0 && (*s == '\'' || *s == '"') && (t = s))
			type = *s;
		if (type != '\'' &&\
		(*s == '$' || (*s == '~' && (s[1] == '/' || s[1] == 0))))
		{
			ft_fill_env(d, &s, &i);
			continue;
		}
		if (*s != type)
			d[i++] = *s;
		else if (t != s)
			type = 0;
		s++;
	}
}
