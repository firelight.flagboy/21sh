/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/28 11:48:42 by eduriez           #+#    #+#             */
/*   Updated: 2018/03/06 12:42:27 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer_parser.h"
#include "libft.h"

static int	ft_create_new_leaf(t_token_list *start_parsing,
							t_token_list *end_parsing,
							t_ast **ast)
{
	t_token	*leaf_content;
	int		cmd_len;

	cmd_len = ft_cmdlen(start_parsing, end_parsing);
	if (cmd_len == 0)
	{
		*ast = NULL;
		return (1);
	}
	else
	{
		leaf_content = ft_get_leaf_content(start_parsing, cmd_len);
		if (leaf_content == NULL)
			return (0);
		*ast = ft_create_ast_node(leaf_content, cmd_len);
	}
	if (*ast == NULL)
	{
		ft_memdel((void**)&leaf_content);
		return (0);
	}
	return (1);
}

static int	ft_create_new_node(t_ast **ast, t_token_list *priority)
{
	t_token	*node_content;
	char	*value;

	node_content = (t_token*)ft_memalloc(sizeof(t_token));
	if (node_content == NULL)
		return (0);
	value = ft_strdup(priority->token.value);
	if (value == NULL)
	{
		ft_memdel((void**)&node_content);
		return (0);
	}
	*node_content = ft_create_token(priority->token.family,
	priority->token.name, value);
	*ast = ft_create_ast_node(node_content, 1);
	if (*ast == NULL)
	{
		ft_memdel((void**)&(node_content->value));
		ft_memdel((void**)&node_content);
		return (0);
	}
	return (1);
}

t_ast		*ft_create_ast_node(t_token *tab_tokens, const int size_tab)
{
	t_ast *new_node;

	new_node = (t_ast*)ft_memalloc(sizeof(t_ast));
	if (new_node == NULL)
		return (NULL);
	new_node->node_tokens = tab_tokens;
	new_node->size_tab = size_tab;
	new_node->left_son = NULL;
	new_node->right_son = NULL;
	return (new_node);
}

int			ft_to_ast(t_token_list *start_parsing,
						t_token_list *end_parsing,
						t_ast **ast)
{
	t_token_list	*max_priority;
	int				ret;

	max_priority = ft_find_max_priority(start_parsing, end_parsing);
	if (max_priority == NULL)
		return (ft_create_new_leaf(start_parsing, end_parsing, ast));
	else
	{
		if (!ft_create_new_node(ast, max_priority))
			return (0);
		ret = ft_to_ast(start_parsing, max_priority, &((*ast)->left_son));
		if (!ret)
			return (0);
		ret = ft_to_ast(max_priority->next, end_parsing, &((*ast)->right_son));
		return (ret);
	}
}
