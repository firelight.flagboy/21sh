/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chterm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 11:16:57 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:30:30 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHTERM_H
# define CHTERM_H

# include "ft_21sh_o_include.h"

/*
** Term
*/
int			ft_get_term_stat(void);
int			ft_change_term_stat(void);
int			ft_get_termcap(void);
void		ft_reset_term(void);
void		ft_setterm(char *term);
#endif
