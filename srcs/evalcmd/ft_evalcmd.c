/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_evalcmd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 11:45:40 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 11:44:47 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

void		ft_exit_code(int *res, char *name)
{
	int	i;

	if (WIFEXITED(*res))
		*res = WEXITSTATUS(*res);
	else if (WIFSIGNALED(*res))
	{
		i = WTERMSIG(*res);
		if (i != SIGINT)
			ft_dprintf(2, "21sh : %s: %s %d\n", name, ft_strsig(i), i);
	}
	else if (WIFSTOPPED(*res))
	{
		i = WSTOPSIG(*res);
		ft_dprintf(2, "21sh : %s: %s\n", name, ft_strsig(i), i);
	}
}

static int	ft_create_here(t_ast *ast, uint *count)
{
	t_token		*node;
	int			size;
	int			i;

	i = -1;
	if (ast == NULL)
		return (0);
	if (ast->node_tokens[0].family == WORD)
	{
		node = ast->node_tokens;
		size = ast->size_tab;
		while (++i < size)
			if (node[i].name == DLESS)
			{
				ft_create_heredoc(node[i + 1].value, *count);
				*count = *count + 1;
			}
	}
	else
	{
		ft_create_here(ast->left_son, count);
		ft_create_here(ast->right_son, count);
	}
	return (0);
}

void		ft_evalcmd(char *cmd)
{
	t_ast	*ast;
	uint	count;
	uint	count1;

	count = 0;
	count1 = 0;
	if (!(ast = ft_get_ast(cmd)))
	{
		free(cmd);
		return ;
	}
	free(cmd);
	ft_create_here(ast, &count);
	*get_res() = ft_ev_ast(ast, &count1);
	ft_rm_heredoc(count);
	ft_delete_ast(ast);
}
