/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reset_term.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 10:16:46 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:34:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

void	ft_reset_term(void)
{
	char	*res;

	if ((res = tgetstr("ke", NULL)))
		tputs(res, 0, ft_putchar_ent);
	if (isatty(0) == 1)
		if (tcsetattr(0, 0, get_term_off()) == -1)
			exit(ft_dprintf(2, "21sh : can't reset the terminal\n"));
}
