/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_21sh_o_include.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 08:57:09 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/23 16:29:58 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_21SH_O_INCLUDE_H
# define FT_21SH_O_INCLUDE_H

# include <stdlib.h>
# include <termios.h>
# include <unistd.h>
# include <term.h>
# include <string.h>
# include <stdio.h>
# include <errno.h>
# include <sys/ioctl.h>
# include <fcntl.h>
# include <limits.h>
# include <dirent.h>
# include <sys/types.h>
# include <sys/stat.h>

typedef struct dirent		t_dirent;
typedef struct stat			t_stat;

#endif
