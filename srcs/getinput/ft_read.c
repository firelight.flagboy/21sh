/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_read.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:21:50 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 09:27:40 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int			*get_len_p(void)
{
	static int	len_promt;

	return (&len_promt);
}

static void	ft_move_down(ssize_t len)
{
	t_cursor	*cursor;
	int			max_line;

	cursor = get_cursor();
	max_line = len / cursor->row_with;
	while (cursor->line < max_line)
	{
		ft_setterm("do");
		cursor->line = cursor->line + 1;
	}
}

static char	*ft_routine_read(int rt, ssize_t len)
{
	char	*s;
	t_listd	**headref;

	headref = get_listd();
	if (rt == -1)
		exit(!!(ft_dprintf(2, "error read : %s\n", strerror(errno))));
	ft_move_down(len);
	if (!(s = ft_listd_to_str(headref)))
		exit(EXIT_FAILURE);
	ft_listd_free(headref);
	return (s);
}

static int	ft_init_read(t_listd ***headref, char *prompt)
{
	*get_len_p() = ft_strlen(prompt);
	resize(0);
	ft_setterm("sc");
	*headref = get_listd();
	get_cursor()->row = *get_len_p();
	get_cursor()->line = 0;
	return (0);
}

char		*ft_read(char *prompt)
{
	char	s[11];
	int		rt;
	int		index;
	int		len;
	t_listd	**headref;

	index = 0;
	len = 0;
	ft_init_read(&headref, prompt);
	ft_dprintf(2, "%s", prompt);
	while ((rt = read(STDIN_FILENO, s, 10)) > 0)
	{
		s[rt] = 0;
		if (rt == 1 && *s == 0x4 && *headref == NULL\
		&& (*headref = ft_str_to_listd(get_cursor()->exit)))
			break ;
		if (ft_process(s, rt, &index, &len))
			break ;
		ft_dysplay(headref, prompt);
	}
	ft_putchar_fd('\n', 1);
	return (ft_routine_read(rt, len));
}
