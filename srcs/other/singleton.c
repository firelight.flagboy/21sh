/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   singleton.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 09:58:37 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/20 10:05:36 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

t_termios	*get_term(void)
{
	static t_termios term;

	return (&term);
}

t_termios	*get_term_off(void)
{
	static t_termios term;

	return (&term);
}

t_cursor	*get_cursor(void)
{
	static t_cursor cursor;

	return (&cursor);
}

t_keystrock	*get_keystrock(void)
{
	static t_keystrock strock;

	return (&strock);
}

int			*get_res(void)
{
	static int	res;

	return (&res);
}
