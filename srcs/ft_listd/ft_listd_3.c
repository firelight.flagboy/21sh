/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 16:10:39 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/19 15:50:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

static void	ft_listd_rm_at(t_listd **headref)
{
	t_listd	*node;

	node = *headref;
	if (node->next)
	{
		node = node->next;
		node->prev = NULL;
		free(*headref);
		*headref = node;
	}
	else
	{
		free(node);
		*headref = NULL;
	}
}

void		ft_listd_remove_at(t_listd **headref, ssize_t index)
{
	t_listd	*prev;
	t_listd	*node;

	index--;
	if (*headref == NULL || index < 0)
		return ;
	if (index == 0)
		return (ft_listd_rm_at(headref));
	prev = *headref;
	node = prev->next;
	while (node && --index && (prev = node))
		node = node->next;
	if (index != 0)
		return ;
	prev->next = node->next;
	if (node->next)
		node->next->prev = prev;
	free(node);
}

t_listd		*ft_str_to_listd(char const *s)
{
	t_listd *head;
	t_listd *node;

	if (*s)
	{
		if (!(head = ft_listd_create_elem(*s)))
			return (NULL);
		s++;
		node = head;
	}
	while (*s)
	{
		if (!(node->next = ft_listd_create_elem(*s)))
			return (NULL);
		node->next->prev = node;
		node = node->next;
		s++;
	}
	return (head);
}
