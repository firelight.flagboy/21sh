/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memrealloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 14:35:45 by eduriez           #+#    #+#             */
/*   Updated: 2017/12/20 12:29:35 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	char	*ft_fill_new_str(char *s1, const char *s2)
{
	char	*new_str;

	new_str = NULL;
	new_str = ft_strnew(ft_strlen(s1) + ft_strlen(s2));
	if (new_str == NULL)
	{
		ft_memdel((void**)&s1);
		return (NULL);
	}
	ft_strcpy(new_str, s1);
	ft_strcat(new_str, s2);
	ft_memdel((void**)&s1);
	return (new_str);
}

static	void	*ft_do_concat(void *s1, const void *s2)
{
	char	*char_s1;
	char	*char_s2;

	char_s1 = (char*)s1;
	char_s2 = (char*)s2;
	if (char_s1 == NULL)
	{
		char_s1 = ft_strdup(char_s2);
		if (char_s1 == NULL)
			return (NULL);
		else
			return (char_s1);
	}
	else
		return (ft_fill_new_str(char_s1, char_s2));
}

void			*ft_memrealloc(void *s1, const void *s2)
{
	if (s1 == NULL && s2 == NULL)
		return (NULL);
	else if (s1 != NULL && s2 == NULL)
		return (s1);
	else
		return (ft_do_concat(s1, s2));
}
