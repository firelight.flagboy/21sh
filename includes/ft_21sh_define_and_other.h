/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_21sh_define_and_other.h                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 08:56:40 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 09:27:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_21SH_DEFINE_AND_OTHER_H
# define FT_21SH_DEFINE_AND_OTHER_H

/*
** Define
*/
# ifndef LEN_PROMPT
#  define LEN_PROMPT 2
# endif

# ifndef HARD_END
#  define HARD_END "\x1b\x4f\x46"
# endif

# ifndef HARD_UP
#  define HARD_UP "\x1b\x4f\x41"
# endif

# ifndef HARD_DOWN
#  define HARD_DOWN "\x1b\x4f\x42"
# endif

# ifndef HARD_SHIFT_PAGE_UP
#  define HARD_SHIFT_PAGE_UP "\x1b\x5b\x35\x7e"
# endif

# ifndef HARD_SHIFT_PAGE_DOWN
#  define HARD_SHIFT_PAGE_DOWN "\x1b\x5b\x36\x7e"
# endif

# ifndef HARD_SHIFT_LEFT
#  define HARD_SHIFT_LEFT "\x1b\x5b\x31\x3b\x32\x44"
# endif

# ifndef HARD_SHIFT_RIGHT
#  define HARD_SHIFT_RIGHT "\x1b\x5b\x31\x3b\x32\x43"
# endif

# ifndef HARD_OPTION_C_LEFT
#  define HARD_OPTION_C_LEFT "\xe2\x89\xa4"
# endif

# ifndef HARD_OPTION_C_RIGHT
#  define HARD_OPTION_C_RIGHT "\xe2\x89\xa5"
# endif

# ifndef HARD_OPTION_P
#  define HARD_OPTION_P "\xcf\x80"
# endif

# ifndef HARD_OPTION_LEFT
#  define HARD_OPTION_LEFT "\x1b\x62"
# endif

# ifndef HARD_OPTION_RIGHT
#  define HARD_OPTION_RIGHT "\x1b\x66"
# endif

/*
** Struct
*/
struct	s_keystrock
{
	char	*left_a;
	char	*right_a;
	char	*upper_a;
	char	*down_a;
	char	*del;
	char	*home;
	char	*end;
};

struct	s_cursor
{
	int		row;
	int		line;
	int		line_with;
	int		row_with;
	int		row_max;
	int		line_max;
	int		index_hist;
	char	*exit;
};
/*
** Enum
*/
enum	e_move
{
	NOP,
	LEFT,
	RIGHT,
	UP,
	DOWN,
	NEXT,
	PREV
};
/*
** Typedef
*/
typedef enum e_move			t_move;
typedef struct s_cursor		t_cursor;
typedef struct s_keystrock	t_keystrock;
typedef struct termios		t_termios;
typedef struct ttysize		t_ttysize;
#endif
