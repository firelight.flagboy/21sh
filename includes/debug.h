/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 15:00:27 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:04:52 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEBUG_H
# define DEBUG_H

# include "lexer_parser.h"
# include "argv.h"
# include "hash.h"

char	*ft_str_token_family(t_token_family val);
char	*ft_str_token_name(t_token_name val);
int		ft_test_lexer(char *s);
int		ft_test_parser(char *s);
int		ft_test_argv(char *s, char **ev);
int		ft_test_hash(char **ev);
#endif
