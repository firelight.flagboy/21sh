/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cut_right.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 13:19:12 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/23 09:47:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static int	ft_rtcutright(t_listd *tmp, int *index)
{
	if (tmp && tmp->prev)
		tmp->prev->next = NULL;
	ft_listd_free(&tmp);
	if (*index == 0)
		*get_listd() = NULL;
	return (0);
}

int			ft_cut_right(int *index, int *len)
{
	char	**clipboard;
	t_listd *node;
	t_listd *tmp;
	int		i;

	clipboard = get_clipboard();
	node = *get_listd();
	if ((*clipboard))
		free((*clipboard));
	if (!((*clipboard) = (char*)malloc((1 + *len - *index) * sizeof(char))))
		return (ft_warnx_errn("clipboard manager", (g_errn = ERR_MALLOC)));
	i = -1;
	while (++i < *index && node)
		node = node->next;
	tmp = node;
	i = -1;
	while (node)
	{
		*len = *len - 1;
		(*clipboard)[++i] = node->c;
		node = node->next;
	}
	(*clipboard)[++i] = 0;
	return (ft_rtcutright(tmp, index));
}
