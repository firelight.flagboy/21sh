/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:16:33 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 12:32:34 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

void	handler_quit(int sig)
{
	signal(sig, handler_quit);
	f_exit(0, 0, 0);
	exit(EXIT_SUCCESS);
}

void	handler_prompt(int sig)
{
	t_cursor *cur;

	signal(sig, handler_prompt);
	ft_listd_free(get_listd());
	ft_setterm("rc");
	ft_move_cursor(RIGHT);
	ft_place_cursor(get_cursor());
	*get_res() = 1;
	cur = get_cursor();
	cur->line = 0;
	cur->row = *get_len_p();
	ft_dprintf(2, "\n");
	ft_head_promt();
	ft_setterm("sc");
	ft_dprintf(2, "$> ");
}

void	handler_pid(int sig)
{
	signal(sig, handler_pid);
	*get_res() = 1;
	kill(g_pid, sig);
	ft_printf("\n");
	ft_change_term_stat();
}

void	resize(int sig)
{
	t_ttysize	ts;
	t_cursor	*cursor;

	(void)sig;
	signal(SIGWINCH, resize);
	ioctl(0, TIOCGSIZE, &ts);
	cursor = get_cursor();
	cursor->row_with = ts.ts_cols;
	cursor->line_with = ts.ts_lines;
	ft_move_cursor(NOP);
}

void	ft_set_signal(void)
{
	int	i;

	i = 0;
	signal(SIGQUIT, handler_quit);
	signal(SIGINT, handler_prompt);
	signal(SIGWINCH, resize);
}
