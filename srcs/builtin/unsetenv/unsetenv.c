/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 14:43:52 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 11:51:37 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int		ft_unsetenv_main(int argc, char **argv, t_env **env)
{
	t_env	*node;

	if (argc > 1)
	{
		if (argc > 2)
			return (ft_warnx_errn("21sh", (g_errn = ERR_ARGTOOMUCH)));
		ft_env_remove_if(env, argv[1]);
		if (ft_strcmp(argv[1], "PATH") == 0)
			ft_refresh_hash();
	}
	node = *env;
	while (node)
	{
		ft_printf("%s=%s\n", node->key, node->value);
		node = node->next;
	}
	return (0);
}
