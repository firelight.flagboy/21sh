/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_cte.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/31 15:17:30 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:24:56 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_len_nb_itoa_st(unsigned int n, int base)
{
	size_t	l;

	l = 0;
	while (n)
	{
		n /= base;
		l++;
	}
	return (l);
}

char			*ft_itoa_st(unsigned int n)
{
	static char	s[33];
	size_t		len;

	len = ft_len_nb_itoa_st(n, 10);
	s[len] = 0;
	len--;
	while ((long)len >= 0)
	{
		s[len] = (n % 10) + '0';
		n /= 10;
		len--;
	}
	return (s);
}
