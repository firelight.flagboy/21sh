/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_token_to_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 15:36:33 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:19:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "debug.h"

static char	*ft_str_token_name_2(t_token_name val)
{
	if (DGREAT == val)
		return ("DGREAT");
	if (GREAT_AND == val)
		return ("GREAT_AND");
	if (PIPE_OP == val)
		return ("PIPE_OP");
	if (OR_OP == val)
		return ("OR_OP");
	if (AND_OP == val)
		return ("AND_OP");
	if (SEMI_COLON_OP == val)
		return ("SEMI_COLON_OP");
	return ("unknow !");
}

char		*ft_str_token_family(t_token_family val)
{
	if (WORD == val)
		return ("WORD");
	if (OP_CTRL == val)
		return ("OP_CTRL");
	if (OP_REDIR == val)
		return ("OP_REDIR");
	return ("unknow !");
}

char		*ft_str_token_name(t_token_name val)
{
	if (NEWLINE_OP == val)
		return ("NEWLINE_OP");
	if (NORMAL_WORD == val)
		return ("NORMAL_WORD");
	if (IO_NUMBER == val)
		return ("IO_NUMBER");
	if (LESS == val)
		return ("LESS");
	if (DLESS == val)
		return ("DLESS");
	if (LESS_AND == val)
		return ("LESS_AND");
	if (GREAT == val)
		return ("GREAT");
	return (ft_str_token_name_2(val));
}
