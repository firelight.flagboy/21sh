/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 12:06:31 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/22 09:59:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

size_t	ft_len_tchar(char **t)
{
	size_t	len;

	len = 0;
	while (t[len])
		len++;
	return (len);
}

int		cd(int argc, char **argv, t_env **environ)
{
	int			uselink;
	size_t		len;

	(void)argc;
	(void)environ;
	argv++;
	uselink = ft_uselink(&argv);
	if (uselink == 1)
		return (1);
	if ((len = ft_len_tchar(argv)) > 1)
		return (ft_warnx_errn("cd", (g_errn = ERR_ARGTOOMUCH)));
	if (len == 0)
		return (ft_move_home(environ, uselink));
	if ((*argv)[0] == '-' && (*argv)[1] == 0)
		return (ft_move_back(environ, uselink));
	else
		return (ft_move_to(environ, *argv, uselink));
	return (0);
}
