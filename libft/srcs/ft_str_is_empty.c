/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_empty.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 13:56:37 by eduriez           #+#    #+#             */
/*   Updated: 2017/11/29 17:26:29 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_str_is_empty(const char *s)
{
	if (s != NULL)
	{
		if (s[0] == '\0')
			return (1);
		else
			return (0);
	}
	return (1);
}
