/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rm_heredoc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 12:25:01 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/09 16:34:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"

static int	ft_init(char ***av, uint count, char *s)
{
	char	**argv;

	if (!(argv = (char**)ft_memalloc((count + 3) * sizeof(char*))))
		return (1);
	argv[count + 2] = NULL;
	ft_strcpy(s, "/tmp/heredoc-");
	argv[0] = ft_strdup("rm");
	argv[1] = ft_strdup("-f");
	*av = argv;
	return (0);
}

int			ft_rm_heredoc(uint count)
{
	char	s[25];
	char	**av;
	pid_t	pid;
	uint	i;

	if (count == 0)
		return (0);
	ft_init(&av, count, s);
	i = -1;
	while (++i < count)
	{
		ft_strcat(s, ft_count_to_str(i));
		av[i + 2] = ft_strdup(s);
		s[13] = 0;
	}
	if ((pid = fork()) == 0)
	{
		if (execve("/bin/rm", av, NULL) < 0)
			ft_error("21sh", "execve error");
	}
	else if (pid == -1)
		ft_warnx("21sh", "fork error");
	ft_free_tchar(av);
	return (0);
}
