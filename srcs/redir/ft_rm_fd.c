/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rm_fd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 13:27:49 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/09 13:31:08 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"

static int	ft_init(char ***av, uint count, char **s)
{
	if (!(*av = (char**)malloc((count + 3) * sizeof(char*))))
		return (1);
	*av[count + 2] = NULL;
	ft_strcpy(*s, "/tmp/fd-");
	*av[0] = ft_strdup("rm");
	*av[1] = ft_strdup("-f");
	return (0);
}

int			ft_rm_fd(uint count)
{
	char	s[20];
	char	**av;
	pid_t	pid;
	uint	i;

	if (count == 0)
		return (0);
	ft_init(&av, count, (char**)&s);
	i = -1;
	while (++i < count)
	{
		ft_strcat(s, ft_count_to_str(i));
		av[i + 2] = ft_strdup(s);
		s[8] = 0;
	}
	if ((pid = fork()) == 0)
	{
		if (execve("/bin/rm", av, NULL) < 0)
			ft_error("21sh", "execve error");
	}
	else if (pid == -1)
		ft_warnx("21sh", "fork error");
	ft_free_tchar(av);
	return (0);
}
