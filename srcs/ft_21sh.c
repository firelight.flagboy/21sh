/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_21sh.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 09:53:10 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 09:30:02 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

void		ft_head_promt(void)
{
	char	buf[PATH_MAX];

	*get_save_cmd() = NULL;
	get_cursor()->exit = "exit";
	get_cursor()->index_hist = -1;
	getcwd(buf, PATH_MAX);
	ft_dprintf(2, "\033[2m"
	"-------------------------------------------\033[0m\n");
	ft_dprintf(2, "%s%s\033[0m\n", (*get_res()) ? "\033[31m" : "\033[32m", buf);
}

void		ft_set_env(void)
{
	t_env	**head;
	char	pth[PATH_MAX];
	char	*s;
	int		i;

	head = get_env();
	if (!(s = ft_getenv(head, "SHLVL")))
		ft_setenv(head, "SHLVL", "1");
	else
	{
		i = ft_atoi(s);
		ft_setenv(head, "SHLVL", ft_itoa_st(i + 1));
	}
	if (!(s = ft_getenv(head, "_")))
	{
		getcwd(pth, PATH_MAX);
		if (!(s = ft_strjoin(pth, "/./21sh")))
			f_exit(0, 0, 0);
		ft_setenv(head, "SHELL", s);
		free(s);
	}
	else
		ft_setenv(head, "SHELL", ft_getenv(head, "_"));
}

int			ft_promt_loop(void)
{
	char	*res;
	char	*tmp;

	while (1)
	{
		ft_head_promt();
		res = ft_read("$> ");
		ft_strdel(get_save_cmd());
		if (*res == 0)
		{
			free(res);
			continue;
		}
		ft_hist_push(get_history(), res);
		tmp = ft_strtrim(res);
		if (!(res = ft_strjoin(tmp, "\n")))
			f_exit(0, 0, 0);
		free(tmp);
		ft_evalcmd(res);
	}
	ft_hist_free(get_history());
	return (0);
}

int			main(int argc, char **argv, char **environ)
{
	char	*nterm;

	if ((*get_is_insteract() = isatty(STDIN_FILENO)) == 0 || argc > 1)
		return (ft_non_interactive_mode(argc, argv, environ));
	if ((nterm = getenv("TERM")) == NULL)
		return (!!ft_dprintf(2, "21sh : TERM name not set\n"));
	if (tgetent(NULL, nterm) == -1)
		return (!!ft_dprintf(2, "21sh : can't get entry to the terminal\n"));
	if (ft_get_term_stat() == -1)
	{
		*get_res() = EXIT_FAILURE;
		f_exit(0, 0, 0);
	}
	if (!(*get_env() = ft_env_tab_list(environ)))
		f_exit(0, 0, 0);
	ft_set_signal();
	ft_set_env();
	ft_setterm("cl");
	ft_promt_loop();
	ft_reset_term();
	return (f_exit(0, 0, 0));
}
