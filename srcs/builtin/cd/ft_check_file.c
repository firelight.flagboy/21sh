/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 14:37:31 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 09:37:39 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int		ft_check_file(char *path)
{
	if (access(path, F_OK))
		return ((int)(g_errn = ERR_NOENT) >= 0);
	if (access(path, X_OK))
		return ((int)(g_errn = ERR_ACCESS) >= 0);
	return (0);
}
