/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_t_hist.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:18:01 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/08 09:31:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "history.h"

t_hist	**get_history(void)
{
	static	t_hist *head;

	return (&head);
}

t_hist	*ft_hist_creat_elem(char *s)
{
	t_hist *node;

	if (!(node = (t_hist*)malloc(sizeof(t_hist))))
		return (NULL);
	node->next = NULL;
	node->cmd = s;
	node->prev = NULL;
	return (node);
}

int		ft_hist_push(t_hist **headref, char *cmd)
{
	t_hist *node;
	t_hist *head;

	if (*cmd == 0)
		return (0);
	head = *headref;
	if (!(node = ft_hist_creat_elem(cmd)))
		return (1);
	node->next = head;
	if (head)
		head->prev = node;
	*headref = node;
	return (0);
}

void	ft_hist_free(t_hist **headref)
{
	t_hist *node;
	t_hist *tmp;

	node = *headref;
	while (node)
	{
		tmp = node;
		free(node->cmd);
		node = node->next;
		free(tmp);
	}
	*headref = NULL;
}
