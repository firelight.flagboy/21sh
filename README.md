# 21sh
21sh project
#
## PDF
- **[link to pdf](https://cdn.intra.42.fr/pdf/pdf/468/21sh.fr.pdf)**
- **[shell](http://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html)**
#
## Man
- **[pipe](http://manpagesfr.free.fr/man/man2/pipe.2.html)**
- **[dup](http://manpagesfr.free.fr/man/man2/dup.2.html)**
- **[ioctl](http://manpagesfr.free.fr/man/man2/ioctl.2.html)**
- **[isatty](http://manpagesfr.free.fr/man/man3/isatty.3.html)**
- **[ttyname](http://manpagesfr.free.fr/man/man3/ttyname.3.html)**
- **[ttyslot](http://manpagesfr.free.fr/man/man3/ttyslot.3.html)**
- **[termios](http://manpagesfr.free.fr/man/man3/termios.3.html)**
- **[termcaps](https://man.cx/termcap(3))**
	- **[example](http://loiclefloch.fr/877-les-termcaps/)**
#
## Help
- **[grammar shell](http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_01)**
- **[shell principe de cmd pdf](http://igm.univ-mlv.fr/~masson/Teaching/PIM-INF3/shell.pdf)**
- **[termcaps tutoriel](http://loiclefloch.fr/877-les-termcaps/)**
- **[termcaps GNU](https://www.gnu.org/software/termutils/manual/termcap-1.3/html_mono/termcap.html)**
#
# Control
| control | description |
| --- | --- |
| `shift`+`left arrow` | move to the previous word |
| `shift`+`right arrow` | move to the next word |
| `left arrow` | move to the previous char |
| `right arrow` | move to the next char |
| `shift`+`Page up` | move to the previous line |
| `shift`+`Page down` | move to the next line |
| `shift`+`home` | move to the begin of the `command` |
| `shift`+`end` | move to the end of the `command` |
| `backspace` | del the previous char |
| `delete` | del the current char |
| `up arrow` | move to the previous `command` that was launch |
| `down arrow` | move to the next `command` that was lauch |
| `option`+`left arrow` | `copy` elem at left of the `cursor` |
| `option`+`right arrow` | `copy` elem at right of the `cursor` |
| `option`+`<` | `cut` elem at left of the `cursor` |
| `option`+`>` | `cut` elem at right of the `cursor` |
| `option`+`p` | `paste` the `clipboard` |
#
# Redirection
| symbol | description |
| --- | --- |
| `[n]>file` | redirect to the file_des `n` or `standar ouput` if not specified to the `file` |
| `[n]<file` | redirect to the content of `file` to the file_des `n` or `standar input` if not specified |
| `[n]>>file` | redirect to the file_des `n` or `standar ouput` if not specified to the `file` (on append mode) |
| `[n]<<word` | redirect the subsequent line read by the shell to the file_des `n` or `standar input` if not specified the redirection stop when you entrer `word` in the subline shell |
| `[n]<&word` | - if `n` not specified use `standar input` as default<br>- if `word` is `-` then close the file_des `n`<br>- if `word` is only digits duplicate the file_des `word` to `n` else redirect the content of `word` to file_des `n` |
| `[n]>&word` | - if `n` not specified use `standar output` as default<br>- if `word` is `-` then close the file_des `n`<br>- if `word` is only digits duplicate the file_des `word` to `n` else redirect the content of `word` to file_des `n`|
