/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_arg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 11:48:07 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/09 08:58:41 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

int		ft_get_env_name(char *d, char *s)
{
	int		i;

	i = 1;
	while (s[i] && ft_isspace(s[i]) == 0\
		&& (s[i] != '"' && s[i] != '\'' && s[i] != '/' && s[i] != '\\'))
	{
		d[i - 1] = s[i];
		i++;
	}
	return (i);
}

char	*ft_get_arg(t_token token)
{
	int		len;
	char	*s;

	s = token.value;
	len = ft_len_arg(s);
	if (!(s = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	s[len] = 0;
	ft_fill_arg(s, token.value);
	return (s);
}
