/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eval_hash.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/07 09:20:48 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 11:38:57 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

static int	ft_wait_hash(pid_t pid, char **av)
{
	int		res;

	g_pid = pid;
	signal(SIGINT, handler_pid);
	if (waitpid(pid, &res, 0) == -1)
		return (ft_warnx("21sh", "waitpid error"));
	ft_change_term_stat();
	ft_setterm("ks");
	ft_exit_code(&res, av[0]);
	ft_free_tchar(av);
	signal(SIGINT, handler_prompt);
	return (res);
}

static int	ft_exe_hash(char **av, t_ast *ast, char *path, uint *c)
{
	int		res;
	pid_t	pid;
	char	**ev;
	t_stat	st;

	res = 1;
	if (ft_access(path, X_OK) || (stat(path, &st) && (g_errn = ERR_STAT))\
	|| (!S_ISREG(st.st_mode) && (g_errn = ERR_ISNOTFILE)))
	{
		ft_warn2_errn("21sh", path, g_errn);
		ft_free_tchar(av);
		return (1);
	}
	if ((pid = fork()) == 0)
	{
		ft_reset_term();
		if (ft_redir(ast->node_tokens, ast->size_tab, c))
			exit(EXIT_FAILURE);
		ev = ft_env_list_tab(get_env());
		if (execve(path, av, ev))
			return (ft_error("21sh", "execve error"));
	}
	else if (pid == -1)
		return (ft_warnx("21sh", "fork error"));
	return (ft_wait_hash(pid, av));
}

static int	ft_hash_exit(char **av)
{
	ft_warn2_errn("21sh", av[0], (g_errn = ERR_NOCMD));
	ft_free_tchar(av);
	return (127);
}

int			ft_eval_hash(char **av, t_ast *ast, uint *c)
{
	t_hash		**hash;
	t_hash		*node;
	uintmax_t	index;

	hash = get_hash();
	index = ft_hashing(av[0]);
	node = hash[index];
	while (node)
	{
		if (ft_strcmp(av[0], node->key) == 0)
			return (ft_exe_hash(av, ast, node->path, c));
		node = node->next;
	}
	ft_refresh_hash();
	hash = get_hash();
	index = ft_hashing(av[0]);
	node = hash[index];
	while (node)
	{
		if (ft_strcmp(av[0], node->key) == 0)
			return (ft_exe_hash(av, ast, node->path, c));
		node = node->next;
	}
	return (ft_hash_exit(av));
}
