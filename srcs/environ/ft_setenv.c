/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 15:48:21 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/09 08:52:07 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "environ.h"

int		ft_setenv(t_env **headref, char *key, char *value)
{
	t_env *node;
	t_env *new;

	if (headref == NULL && (g_errn = ERR_RANGE))
		return (1);
	node = *headref;
	while (node)
	{
		if (ft_strcmp(node->key, key) == 0)
		{
			if (node->value)
				free(node->value);
			if (!(node->value = ft_strdup(value)) && (g_errn == ERR_MALLOC))
				return (1);
			return (0);
		}
		node = node->next;
	}
	if (!(new = ft_env_new(key, value))\
	&& (g_errn = ERR_MALLOC))
		return (1);
	new->next = *headref;
	*headref = new;
	return (0);
}
