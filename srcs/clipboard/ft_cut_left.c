/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cut_left.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 13:19:14 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 09:16:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static void	ft_clear_cut_left(t_cursor *cursor)
{
	int	line;

	line = 0;
	ft_dprintf(0, "\33[2K");
	while (line < cursor->line)
	{
		ft_dprintf(0, "\33[2K");
		ft_setterm("up");
		line++;
	}
}

static void	ft_rz_cut_left(size_t len)
{
	size_t i;

	i = 0;
	while (i < len)
	{
		i++;
		ft_move_cursor(LEFT);
	}
}

static int	ft_rt_cutleft(t_listd *node, int *index, int *len)
{
	if (node && node->prev)
		node->prev->next = NULL;
	ft_listd_free(get_listd());
	*get_listd() = node;
	*len = *len - *index;
	*index = 0;
	return (0);
}

int			ft_cut_left(int *index, int *len)
{
	char	**clipboard;
	int		i;
	t_listd *node;

	clipboard = get_clipboard();
	node = *get_listd();
	if ((*clipboard))
		free((*clipboard));
	if (!((*clipboard) = (char*)malloc((*index + 1) * sizeof(char))))
		return (ft_warnx_errn("clipboard manager", (g_errn = ERR_MALLOC)));
	i = -1;
	while (++i < *index && node)
	{
		(*clipboard)[i] = node->c;
		node = node->next;
	}
	(*clipboard)[i] = 0;
	ft_clear_cut_left(get_cursor());
	ft_rz_cut_left(*index);
	return (ft_rt_cutleft(node, index, len));
}
