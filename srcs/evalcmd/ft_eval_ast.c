/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eval_ast.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 13:32:27 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 10:52:11 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

int			ft_eval_cmd(t_ast *ast, uint *count)
{
	int		argc;
	char	**argv;

	if ((argc = ft_get_argv(&argv, ast->node_tokens, ast->size_tab)) == -1)
		return (ft_warnx("build argv", "malloc error"));
	if (argv == NULL || argc <= 0)
	{
		ft_dprintf(2, "21sh : %s : command not found\n",
		ast->node_tokens[0].value);
		if (argv)
			ft_free_tchar(argv);
		return (1);
	}
	if (ft_strchr(argv[0], '/'))
		return (ft_eval_direct_cmd(argv, ast, count));
	else if (ft_eval_built(argv, &argc, ast, count) == 0)
		return (argc);
	else
		return (ft_eval_hash(argv, ast, count));
	return (1);
}

static int	ft_eval_ctrl(t_ast *a, int type, uint *c)
{
	if (PIPE_OP == type)
		return (ft_pipe_cmd(a, c));
	if (OR_OP == type)
	{
		return (ft_ev_ast(a->left_son, c) == 0\
			|| ft_ev_ast(a->right_son, c) == 0);
	}
	if (AND_OP == type)
	{
		return (ft_ev_ast(a->left_son, c) == 0\
			&& ft_ev_ast(a->right_son, c) == 0);
	}
	if (SEMI_COLON_OP == type)
	{
		ft_ev_ast(a->left_son, c);
		return (ft_ev_ast(a->right_son, c));
	}
	return (0);
}

int			ft_ev_ast(t_ast *ast, uint *count)
{
	int type;

	if (ast == NULL)
		return (0);
	type = ast->node_tokens[0].family;
	if (type == WORD)
		return (ft_eval_cmd(ast, count));
	else if (type == OP_CTRL)
		return (ft_eval_ctrl(ast, ast->node_tokens[0].name, count));
	return (0);
}
