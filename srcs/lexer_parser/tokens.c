/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokens.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 21:00:56 by eduriez           #+#    #+#             */
/*   Updated: 2018/03/12 08:36:16 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer_parser.h"
#include "libft.h"

int				ft_cmdlen(t_token_list *begin_list, t_token_list *end_list)
{
	int	i;

	i = 0;
	while (begin_list != end_list)
	{
		begin_list = begin_list->next;
		i++;
	}
	return (i);
}

t_token			ft_create_token(const t_token_family family,
								const t_token_name name,
								char *value)
{
	t_token token;

	token.family = family;
	token.name = name;
	token.value = value;
	return (token);
}

int				ft_add_token(const t_token token, t_token_list **list_tokens)
{
	t_token_list	*new_elem;
	t_token_list	*begin_list;

	new_elem = (t_token_list*)ft_memalloc(sizeof(t_token_list));
	if (new_elem == NULL)
		return (0);
	new_elem->token = token;
	new_elem->next = NULL;
	begin_list = *list_tokens;
	if (*list_tokens == NULL)
		*list_tokens = new_elem;
	else
	{
		while (begin_list->next != NULL)
			begin_list = begin_list->next;
		begin_list->next = new_elem;
	}
	return (1);
}

t_token_list	*ft_find_max_priority(t_token_list *begin_list,
									t_token_list *end_list)
{
	t_token_name token_name;
	t_token_list *elem_found;

	token_name = NORMAL_WORD;
	elem_found = NULL;
	while (begin_list != end_list)
	{
		if (begin_list->token.family == OP_CTRL
			&& begin_list->token.name > token_name)
		{
			token_name = begin_list->token.name;
			elem_found = begin_list;
		}
		begin_list = begin_list->next;
	}
	return (elem_found);
}
