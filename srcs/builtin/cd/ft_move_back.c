/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_back.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 14:58:32 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 11:59:52 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

static int	ft_end_back(t_env **ev, char *path, int uselnk)
{
	char	buf[PATH_MAX];

	getcwd(buf, PATH_MAX);
	if (uselnk == 0)
		ft_setenv(ev, "PWD", buf);
	else
		ft_setenv(ev, "PWD", path);
	free(path);
	return (0);
}

int			ft_move_back(t_env **ev, int uselnk)
{
	char	buf[PATH_MAX];
	char	*old;
	char	*cur;
	char	*path;

	if (!(old = ft_getenv(ev, "OLDPWD")))
		return (ft_warnx_errn("cd", (g_errn = ERR_NOOLDPWD)));
	if (!ft_getenv(ev, "PWD") || !ft_strcpy(buf, ft_getenv(ev, "PWD")))
		getcwd(buf, PATH_MAX);
	if (!(cur = ft_getenv(ev, "PWD")))
		cur = buf;
	path = ft_path_normalize(old);
	if (ft_check_file(path) || ft_check_dir(path))
	{
		ft_warn2_errn("cd", path, g_errn);
		free(path);
		return (1);
	}
	if (uselnk == 0)
		ft_setenv(ev, "OLDPWD", buf);
	else
		ft_setenv(ev, "OLDPWD", cur);
	chdir(path);
	return (ft_end_back(ev, path, uselnk));
}
