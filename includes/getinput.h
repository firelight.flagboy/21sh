/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getinput.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 10:12:15 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:17:49 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GETINPUT_H
# define GETINPUT_H

# include "ft_listd.h"
# include "ft_21sh_define_and_other.h"

int			ft_step_word(int move, int *index, int *len);
int			ft_move_cursor(int move);
int			ft_move_cursor_multi(int move, int *index, int *len);
void		ft_display_at(t_listd **headref, int at);
int			ft_key(char *input, int *index, int *len);
char		*ft_read(char *prompt);
int			ft_putchar_sp(int c, int index);
int			ft_process(char *input, int rt, int *index, int *len);
void		ft_backspace(void);
void		ft_dysplay(t_listd **headref, char *prompt);
int			ft_save_cmd(void);
int			ft_reset_cmd(int *index, int *len, t_cursor *cr);
#endif
