/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_access.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/07 09:25:00 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/07 10:03:00 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "other.h"

int		ft_access(char *path, int mode)
{
	if (access(path, F_OK))
	{
		g_errn = ERR_NOENT;
		return (1);
	}
	if (access(path, mode))
	{
		g_errn = ERR_ACCESS;
		return (1);
	}
	return (0);
}
