/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env_call_prog.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 12:04:22 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 13:40:22 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

static int	ft_exe_direct(char *path, char **av, t_env **ev)
{
	pid_t	pid;
	int		res;
	char	**env;

	if (ft_access(path, X_OK))
		return (ft_warn2_errn("21sh", path, g_errn));
	if ((pid = fork()) == 0)
	{
		env = ft_env_list_tab(ev);
		if (execve(path, av, env))
			return (ft_error("21sh", "execve error"));
	}
	else if (pid == -1)
		return (ft_warnx("21sh", "fork error"));
	g_pid = pid;
	signal(SIGINT, handler_pid);
	if (waitpid(pid, &res, 0) == -1)
		return (ft_warnx("21sh", "waitpid error"));
	signal(SIGINT, handler_prompt);
	return (res);
}

static int	ft_exe_built(char **av, t_env **ev, int res)
{
	t_built	*built;
	int		argc;

	built = get_tbuilt();
	argc = ft_len_tchar(av);
	return (built[res].built(argc, av, ev));
}

static int	ft_exe_hash(char **av, t_env **ev)
{
	t_hash		**hash;
	t_hash		*node;
	uintmax_t	index;

	hash = get_hash();
	index = ft_hashing(av[0]);
	node = hash[index];
	while (node)
	{
		if (ft_strcmp(av[0], node->key) == 0)
			return (ft_exe_direct(node->path, av, ev));
		node = node->next;
	}
	ft_refresh_hash();
	hash = get_hash();
	index = ft_hashing(av[0]);
	node = hash[index];
	while (node)
	{
		if (ft_strcmp(av[0], node->key) == 0)
			return (ft_exe_direct(node->path, av, ev));
		node = node->next;
	}
	return (ft_warn2_errn("21sh", av[0], (g_errn = ERR_NOCMD)));
}

static int	ft_exe_cmd(char **av, t_env **ev)
{
	int		res;

	if (ft_strchr(av[0], '/'))
		return (ft_exe_direct(av[0], av, ev));
	else if ((res = ft_isbuilt(av[0])) >= 0)
		return (ft_exe_built(av, ev, res));
	else
		return (ft_exe_hash(av, ev));
}

int			ft_env_call_prog(char **av, t_env **ev, int *res)
{
	pid_t	pid;

	if ((pid = fork()) == 0)
	{
		exit(ft_exe_cmd(av, ev));
	}
	else if (pid == -1)
		return (EXIT_FAILURE);
	g_pid = pid;
	signal(SIGINT, handler_pid);
	if (waitpid(pid, res, 0) == -1)
		return (ft_warnx("%s : waitpid error\n", av[0]));
	signal(SIGINT, handler_prompt);
	return (0);
}
