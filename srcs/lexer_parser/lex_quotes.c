/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lex_quotes.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 21:06:38 by eduriez           #+#    #+#             */
/*   Updated: 2018/02/27 17:51:44 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer_parser.h"
#include "libft.h"
#include "ft_printf.h"

int	ft_lex_simple_quotes(const char *cmd_user, int *nb_chars_in_word)
{
	*nb_chars_in_word += 1;
	if (ft_strchr(cmd_user + *nb_chars_in_word, '\'') != NULL)
	{
		while (cmd_user[*nb_chars_in_word] != '\'')
			*nb_chars_in_word += 1;
		*nb_chars_in_word += 1;
		return (1);
	}
	ft_dprintf(2, "Lexing error near '%c'\n", '\'');
	return (0);
}

int	ft_lex_double_quotes(const char *cmd_user, int *nb_chars_in_word)
{
	*nb_chars_in_word += 1;
	if (ft_strchr(cmd_user + *nb_chars_in_word, '"') != NULL)
	{
		while (cmd_user[*nb_chars_in_word] != '"')
			*nb_chars_in_word += 1;
		*nb_chars_in_word += 1;
		return (1);
	}
	ft_dprintf(2, "Lexing error near '%c'\n", '"');
	return (0);
}
