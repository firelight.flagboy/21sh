/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer_parser.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 21:28:08 by eduriez           #+#    #+#             */
/*   Updated: 2018/03/06 12:26:17 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEXER_PARSER_H
# define LEXER_PARSER_H

typedef enum		e_token_family
{
	WORD,
	OP_CTRL,
	OP_REDIR
}					t_token_family;

typedef	enum		e_token_name
{
	NEWLINE_OP,
	NORMAL_WORD,
	IO_NUMBER,
	LESS,
	DLESS,
	LESS_AND,
	GREAT,
	DGREAT,
	GREAT_AND,
	PIPE_OP,
	OR_OP,
	AND_OP,
	SEMI_COLON_OP
}					t_token_name;

typedef struct		s_token
{
	t_token_family	family;
	t_token_name	name;
	char			*value;
}					t_token;

typedef struct		s_token_list
{
	t_token				token;
	struct s_token_list	*next;
}					t_token_list;

typedef struct		s_ast
{
	t_token			*node_tokens;
	int				size_tab;
	struct s_ast	*left_son;
	struct s_ast	*right_son;
}					t_ast;

typedef int			(*t_lex_word_func)(const char*, int*, t_token_list**);

int					ft_is_start_op(const char c);
int					ft_isspace_separator(const char c);
t_token_list		*ft_lex_cmd(const char *cmd_user);

int					ft_process_word(const char *cmd_user,
									int *i,
									t_token_list **list_tokens);

int					ft_lex_quotes(const char *start_quote,
								int *nb_chars_in_word);

int					ft_lex_simple_quotes(const char *start_quote,
										int *nb_chars_in_word);

int					ft_lex_double_quotes(const char *start_quote,
										int *nb_chars_in_word);

t_token				ft_create_token(const t_token_family family,
									const t_token_name name,
									char *value);

int					ft_add_token(const t_token token,
								t_token_list **list_tokens);

void				ft_delete_list(t_token_list *list_tokens);
void				ft_delete_ast(t_ast *ast);
t_lex_word_func		ft_get_lex_word_function(const char start_op);

int					ft_manage_and_operators(const char *cmd_user,
											int *op_position,
											t_token_list **list_tokens);

int					ft_manage_or_operators(const char *cmd_user,
										int *op_position,
										t_token_list **list_tokens);

int					ft_manage_semicolon_operators(const char *cmd_user,
												int *op_position,
												t_token_list **list_tokens);

int					ft_manage_newline_operator(const char *cmd_user,
											int *op_position,
											t_token_list **list_tokens);

int					ft_manage_less_operators(const char *cmd_user,
											int *op_position,
											t_token_list **list_tokens);

int					ft_manage_great_operators(const char *cmd_user,
											int *op_position,
											t_token_list **list_tokens);

t_token_list		*ft_find_max_priority(t_token_list *begin_list,
										t_token_list *end_list);

int					ft_cmdlen(t_token_list *begin_list, t_token_list *end_list);
int					ft_is_ionumber(const char *str);

t_token				*ft_get_leaf_content(t_token_list *begin_cmd,
										const int size_tab);

t_ast				*ft_create_ast_node(t_token *tab_tokens,
										const int size_tab);

int					ft_to_ast(t_token_list *start_parsing,
							t_token_list *end_parsing,
							t_ast **ast);

t_ast				*ft_get_ast(const char *cmd_user);

int					ft_is_valid_cmd(t_token_list *begin_list);

#endif
