/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_test_lexer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 15:08:57 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:31:02 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "debug.h"

static int	ft_process_test_lexer(char *s)
{
	t_token_list	*node;
	t_token_list	*head;
	t_token			token;

	if (!(head = ft_lex_cmd(s)))
		exit(EXIT_FAILURE);
	free(s);
	node = head;
	while (node)
	{
		token = node->token;
		ft_printf("token family : %-8s : token name : %-13s : value : %s\n",\
		ft_str_token_family(token.family), ft_str_token_name(token.name),\
		(token.value[0] == '\n') ? "newline" : token.value);
		node = node->next;
	}
	ft_delete_list(head);
	return (0);
}

int			ft_test_lexer(char *s)
{
	size_t	l;
	size_t	fl;
	int		addnl;
	char	*tmp;

	l = ft_strlen(s);
	addnl = 0;
	if (s[l - 1] != '\n')
	{
		addnl = 1;
		fl = l + 1;
	}
	else
		fl = l;
	if (!(tmp = (char*)malloc((fl + 1) * sizeof(char))))
		exit(EXIT_FAILURE);
	ft_strcpy(tmp, s);
	if (addnl)
		ft_strcat(tmp, "\n");
	return (ft_process_test_lexer(tmp));
}
