/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_paste_at.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 13:19:08 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 12:09:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static void		ft_link_end(t_listd *node, t_listd *to_add)
{
	while (to_add->next)
		to_add = to_add->next;
	to_add->next = node->next;
	if (node->next)
		node->next->prev = to_add;
}

static void		ft_move_to_end(size_t len, int *index)
{
	size_t	i;

	i = 0;
	*index = *index + len;
	while (i < len)
	{
		i++;
		ft_move_cursor(RIGHT);
	}
}

static int		ft_rt_null(int *index, int *len, t_listd *to_add)
{
	int	i;

	*get_listd() = to_add;
	*len = ft_listd_len_list(*get_listd());
	*index = *len;
	i = 0;
	while (i < *len)
	{
		ft_move_cursor(RIGHT);
		i++;
	}
	return (0);
}

int				ft_paste_at(int *index, int *len)
{
	char	**clipboard;
	t_listd	*node;
	t_listd	*to_add;
	int		i;

	clipboard = get_clipboard();
	node = *get_listd();
	if (*clipboard == NULL || **clipboard == 0)
		return (0);
	if (!(to_add = ft_str_to_listd(*clipboard)))
		return (ft_warnx_errn("clipboard manager", (g_errn = ERR_MALLOC)));
	i = 0;
	if (node == NULL)
		return (ft_rt_null(index, len, to_add));
	while (++i < *index && node->next)
		node = node->next;
	ft_link_end(node, to_add);
	node->next = to_add;
	to_add->prev = node;
	ft_move_to_end(ft_listd_len_list(*get_listd()) - *len, index);
	*len = ft_listd_len_list(*get_listd());
	return (0);
}
