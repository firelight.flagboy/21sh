/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipe.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/08 16:18:29 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:36:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

static void	ft_redir_fd(t_fd old, t_fd new)
{
	if (old == new)
		return ;
	if (dup2(old, new) != -1)
		close(old);
}

int			ft_pipe_fork(t_ast *ast, t_fd input, uint *count)
{
	pid_t	pid;
	t_pipe	pfd;
	int		st;

	pipe(pfd);
	if ((pid = fork()) == 0)
	{
		close(pfd[READ]);
		ft_redir_fd(input, STDIN_FILENO);
		ft_redir_fd(pfd[WRITE], STDOUT_FILENO);
		exit(ft_eval_cmd(ast->left_son, count));
	}
	else if (pid == -1)
		return (1);
	else
	{
		while (waitpid(pid, &st, WNOHANG) == -1)
			continue;
		close(pfd[WRITE]);
		close(input);
		ft_exec_pipe(ast->right_son, pfd[0], count);
		return (st);
	}
}

int			ft_exec_pipe(t_ast *ast, t_fd input, uint *count)
{
	if (ast->left_son == NULL)
	{
		ft_redir_fd(input, STDIN_FILENO);
		exit(ft_eval_cmd(ast, count));
	}
	else
	{
		return (ft_pipe_fork(ast, input, count));
	}
	return (0);
}

int			ft_pipe_cmd(t_ast *ast, uint *count)
{
	int		st;
	pid_t	pid;

	if ((pid = fork()) == 0)
		exit(ft_exec_pipe(ast, STDIN_FILENO, count));
	else if (pid == -1)
		return (1);
	else
	{
		g_pid = pid;
		signal(SIGINT, handler_pid);
		if (waitpid(pid, &st, 0) == -1)
			return (1);
		ft_change_term_stat();
		ft_setterm("ks");
		ft_exit_code(&st, "pipe");
		signal(SIGINT, handler_prompt);
		return (st);
	}
	return (0);
}
