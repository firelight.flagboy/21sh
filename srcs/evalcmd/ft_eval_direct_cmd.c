/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eval_direct_cmd.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/07 09:20:56 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 11:39:21 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

static int	ft_wait_direct_cmd(pid_t pid, char **av)
{
	int	res;

	g_pid = pid;
	signal(SIGINT, handler_pid);
	if (waitpid(pid, &res, 0) == -1)
		return (ft_warnx("21sh", "waitpid error"));
	ft_change_term_stat();
	ft_setterm("ks");
	ft_exit_code(&res, av[0]);
	ft_free_tchar(av);
	signal(SIGINT, handler_prompt);
	return (res);
}

int			ft_eval_direct_cmd(char **av, t_ast *ast, uint *count)
{
	pid_t	pid;
	int		res;
	char	**ev;
	t_stat	st;

	res = 1;
	if (ft_access(av[0], X_OK) || (stat(av[0], &st) && (g_errn = ERR_STAT))\
	|| (!S_ISREG(st.st_mode) && (g_errn = ERR_ISNOTFILE)))
	{
		ft_warn2_errn("21sh", av[0], g_errn);
		ft_free_tchar(av);
		return (1);
	}
	if ((pid = fork()) == 0)
	{
		ft_reset_term();
		if (ft_redir(ast->node_tokens, ast->size_tab, count))
			exit(EXIT_FAILURE);
		ev = ft_env_list_tab(get_env());
		if (execve(av[0], av, ev))
			return (ft_error("21sh", "execve error"));
	}
	else if (pid == -1)
		return (ft_warnx("21sh", "fork error"));
	return (ft_wait_direct_cmd(pid, av));
}
