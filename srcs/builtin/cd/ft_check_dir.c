/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_dir.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 15:09:17 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/21 15:32:42 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int		ft_check_dir(char *path)
{
	struct stat st;

	if (stat(path, &st) && (g_errn = ERR_STAT))
		return (1);
	if (S_ISREG(st.st_mode) && (g_errn = ERR_ISREG))
		return (1);
	if (S_ISDIR(st.st_mode) && (g_errn = ERR_ISDIR))
		return (0);
	if (S_ISCHR(st.st_mode) && (g_errn = ERR_ISCHR))
		return (1);
	if (S_ISBLK(st.st_mode) && (g_errn = ERR_ISBLK))
		return (1);
	if (S_ISFIFO(st.st_mode) && (g_errn = ERR_ISFIFO))
		return (1);
	if (S_ISLNK(st.st_mode) && (g_errn = ERR_ISLNK))
		return (1);
	if (S_ISSOCK(st.st_mode) && (g_errn = ERR_ISSOCK))
		return (1);
	return (0);
}
