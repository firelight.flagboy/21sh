/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_whitespaces.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/26 12:39:16 by eduriez           #+#    #+#             */
/*   Updated: 2018/01/26 12:57:08 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int			ft_count_words(const char *str)
{
	int	i;
	int	number_words;

	i = 0;
	number_words = 0;
	while (str[i] != '\0')
	{
		if (!ft_isspace(str[i]))
		{
			while (!ft_isspace(str[i]) && str[i] != '\0')
				i++;
			number_words++;
		}
		else
		{
			while (ft_isspace(str[i]) && str[i] != '\0')
				i++;
		}
	}
	return (number_words);
}

static char			*ft_fill_word(const char *string_to_split,
								unsigned int *start_word)
{
	size_t				size_str;
	unsigned	int		i;
	char				*new_str;

	new_str = NULL;
	i = *start_word;
	size_str = 0;
	while (!ft_isspace(string_to_split[i]) && string_to_split[i] != '\0')
		i++;
	size_str = (size_t)i - *start_word;
	new_str = ft_strsub(string_to_split, *start_word, size_str);
	if (ft_isspace(string_to_split[i]))
	{
		while (ft_isspace(string_to_split[i]))
			i++;
		*start_word = i;
	}
	return (new_str);
}

static char			**ft_do_split(const char *string_to_split, int number_words)
{
	int					i;
	char				**string_splitted;
	unsigned	int		start_word;

	start_word = 0;
	i = 0;
	string_splitted = (char**)ft_memalloc((number_words + 1) * sizeof(char*));
	if (string_splitted == NULL)
		return (NULL);
	else
	{
		while (ft_isspace(string_to_split[start_word]))
			start_word++;
		while (i < number_words)
		{
			string_splitted[i] = ft_fill_word(string_to_split, &start_word);
			i++;
		}
	}
	return (string_splitted);
}

char				**ft_strsplit_whitespaces(const char *s)
{
	int		number_words;
	char	**string_splitted;

	string_splitted = NULL;
	number_words = 0;
	if (s == NULL)
		return (NULL);
	else
	{
		number_words = ft_count_words(s);
		string_splitted = ft_do_split(s, number_words);
		if (string_splitted == NULL)
			return (NULL);
		string_splitted[number_words] = 0;
	}
	return (string_splitted);
}
