/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_to.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 14:58:34 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 11:56:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

char		*ft_rt_move_to(char **p, char *pwd, char **npwd)
{
	char	*s;
	char	*t;
	size_t	len;

	s = *p;
	len = ft_strlen(pwd);
	while ((ft_strncmp(s, "../", 3) == 0 || ft_strcmp(s, "..") == 0) && len > 0)
	{
		if (ft_strcmp(s, "..") == 0)
			s += 2;
		else
			s += 3;
		t = ft_strrchr(pwd, '/');
		*t = 0;
	}
	if (!(t = ft_strjoin(pwd, "/")))
		return ((ft_warn2_errn("cd", *p, (g_errn = ERR_MALLOC))) ? NULL : NULL);
	*p = s;
	if (!(*npwd = ft_strjoin(t, s)))
		return ((ft_warn2_errn("cd", *p, (g_errn = ERR_MALLOC))) ? NULL : NULL);
	ft_trim_backslash(*npwd);
	free(t);
	return (t);
}

static int	ft_end(int uselnk, char *npwd, t_env **environ)
{
	char	*s;
	char	pwd[PATH_MAX];

	s = ft_path_normalize(npwd);
	if (uselnk == 0)
		ft_setenv(environ, "PWD", getcwd(pwd, PATH_MAX));
	else
		ft_setenv(environ, "PWD", s);
	free(s);
	free(npwd);
	return (0);
}

int			ft_move_to(t_env **ev, char *path, int uselnk)
{
	char	pwd[PATH_MAX];
	char	*npwd;
	char	*s;
	char	*t;

	if (path[0] == '/')
		return (ft_move_to_abs(ev, path, uselnk));
	if (!ft_getenv(ev, "PWD") || !ft_strcpy(pwd, ft_getenv(ev, "PWD")))
		getcwd(pwd, PATH_MAX);
	ft_strcat(pwd, "/");
	if (!(npwd = ft_strjoin(pwd, path)))
		return (1);
	if (ft_check_file(path) || ft_check_dir(path))
	{
		ft_warn2_errn("cd", npwd, g_errn);
		free(npwd);
		return (1);
	}
	s = path;
	if (uselnk == 0 || !(t = ft_getenv(ev, "PWD")))
		ft_setenv(ev, "OLDPWD", pwd);
	else
		ft_setenv(ev, "OLDPWD", t);
	chdir(npwd);
	return (ft_end(uselnk, npwd, ev));
}
