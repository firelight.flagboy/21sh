/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 13:31:42 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 09:11:16 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_H
# define BUILTIN_H

# include "ft_21sh_o_include.h"
# include "ft_printf.h"
# include "hash.h"
# include "libft.h"
# include "evalcmd.h"
# include "my_errno.h"
# include "environ.h"

/*
** echo()
*/
int			echo(int argc, char **argv, t_env **env);
/*
** exit()
*/
int			f_exit(int argc, char **argv, t_env **environ);
/*
** cd()
*/
int			ft_end_slash(char *path);
char		*ft_path_normalize(char *path);
char		*ft_trim_backslash(char *path);
int			cd(int argc, char **argv, t_env **environ);
int			ft_uselink(char ***argv);
int			ft_check_file(char *path);
int			ft_check_dir(char *path);
int			ft_move_home(t_env **environ, int uselnk);
int			ft_move_back(t_env **environ, int uselnk);
int			ft_move_to(t_env **environ, char *path, int uselnk);
int			ft_move_to_abs(t_env **environ, char *path, int uselnk);
/*
** env()
*/
int			ft_isbuilt(char *s);
int			ft_env_call_prog(char **av, t_env **ev, int *res);
int			env(int argc, char **argv, t_env **env);
/*
** setenv()
*/
int			ft_setenv_main(int argc, char **argv, t_env **env);
/*
** unsetenv()
*/
int			ft_unsetenv_main(int argc, char **argv, t_env **env);
#endif
