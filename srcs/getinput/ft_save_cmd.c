/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_save_cmd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/13 14:13:29 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:21:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int		ft_save_cmd(void)
{
	if (*get_save_cmd())
		ft_strdel(get_save_cmd());
	*get_save_cmd() = ft_listd_to_str(get_listd());
	return (0);
}

int		ft_reset_cmd(int *index, int *len, t_cursor *cr)
{
	int			i;

	i = -1;
	while (++i < *len)
		ft_move_cursor(LEFT);
	*index = 0;
	if (*get_listd())
		ft_listd_free(get_listd());
	if (*get_save_cmd())
	{
		*get_listd() = ft_str_to_listd(*get_save_cmd());
		*len = ft_strlen(*get_save_cmd());
		i = -1;
		while (++i < *len)
			ft_move_cursor(RIGHT);
		*index = *len;
	}
	cr->index_hist = cr->index_hist - 1;
	return (0);
}
