/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_to_abs.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 09:49:49 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 11:59:20 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

static int	ft_end_abs(t_env **ev, char *path, int uselnk)
{
	char	pwd[PATH_MAX];

	getcwd(pwd, PATH_MAX);
	path = ft_path_normalize(path);
	if (uselnk == 0)
		ft_setenv(ev, "PWD", pwd);
	else
		ft_setenv(ev, "PWD", path);
	free(path);
	return (0);
}

int			ft_move_to_abs(t_env **ev, char *path, int uselnk)
{
	char	pwd[PATH_MAX];
	char	path2[PATH_MAX];
	int		len;
	char	*t;

	if (!ft_getenv(ev, "PWD") || !ft_strcpy(pwd, ft_getenv(ev, "PWD")))
		getcwd(pwd, PATH_MAX);
	if (ft_check_file(path) || ft_check_dir(path))
		return (ft_warn2_errn("cd", path, g_errn));
	ft_strcpy(path2, path);
	len = ft_strlen(path2);
	if (path2[len - 1] != '/')
		ft_strcat(path2, "/");
	if (uselnk == 0 || !(t = ft_getenv(ev, "PWD")))
		ft_setenv(ev, "OLDPWD", pwd);
	else
		ft_setenv(ev, "OLDPWD", t);
	chdir(path);
	getcwd(pwd, PATH_MAX);
	return (ft_end_abs(ev, path2, uselnk));
}
