/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eval_built.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/07 09:19:56 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 11:05:58 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

static int	ft_save_fd(t_fd save[3])
{
	save[0] = dup(0);
	save[1] = dup(1);
	save[2] = dup(2);
	return (0);
}

static int	ft_reset_fd(t_fd save[3])
{
	close(0);
	close(1);
	close(2);
	dup2(save[0], 0);
	dup2(save[1], 1);
	dup2(save[2], 2);
	close(save[0]);
	close(save[1]);
	close(save[2]);
	return (0);
}

int			ft_eval_built(char **argv, int *rt, t_ast *ast, uint *count)
{
	t_built	*built;
	t_fd	save[3];
	int		i;
	char	argc;

	built = get_tbuilt();
	i = -1;
	while (built[++i].name)
		if (ft_strcmp(argv[0], built[i].name) == 0)
		{
			ft_save_fd(save);
			argc = ft_len_tchar(argv);
			if (ft_redir(ast->node_tokens, ast->size_tab, count) && (*rt = 1))
			{
				ft_free_tchar(argv);
				return (0);
			}
			*rt = built[i].built(argc, argv, get_env());
			ft_free_tchar(argv);
			return (ft_reset_fd(save));
		}
	return (1);
}
