/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   history.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:11:24 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:28:40 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HISTORY_H
# define HISTORY_H

# include "ft_21sh_o_include.h"
# include "ft_21sh_define_and_other.h"
# include "libft.h"

typedef struct		s_hist
{
	char			*cmd;
	int				index;
	struct s_hist	*next;
	struct s_hist	*prev;
}					t_hist;

t_hist				**get_history(void);
t_hist				*ft_hist_creat_elem(char *s);
int					ft_hist_push(t_hist **headref, char *cmd);
void				ft_hist_free(t_hist **headref);
void				ft_get_history\
	(int mode, t_cursor *cr, int *index, int *len);
#endif
