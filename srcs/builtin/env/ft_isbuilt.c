/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isbuilt.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/12 13:32:31 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 13:32:55 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int	ft_isbuilt(char *s)
{
	t_built	*node;
	int		i;

	node = get_tbuilt();
	i = -1;
	while (node[++i].name)
		if (ft_strcmp(node[i].name, s) == 0)
			return (i);
	return (-1);
}
