/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_copy_left.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 13:19:05 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/23 09:46:05 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int		ft_copy_left(int *index, int *len)
{
	char	**clipboard;
	int		i;
	t_listd	**head;
	t_listd *node;

	(void)len;
	clipboard = get_clipboard();
	head = get_listd();
	node = *head;
	if ((*clipboard))
		free((*clipboard));
	if (!((*clipboard) = (char*)malloc((*index + 1) * sizeof(char))))
		return (ft_warnx_errn("clipboard manager", (g_errn = ERR_MALLOC)));
	(*clipboard)[*index] = 0;
	i = -1;
	while (++i < *index && node)
	{
		(*clipboard)[i] = node->c;
		node = node->next;
	}
	return (0);
}
