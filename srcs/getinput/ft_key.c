/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:22:47 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:23:55 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static int	ft_clipboard(char *input, int *index, int *len)
{
	if (ft_strcmp(HARD_OPTION_LEFT, input) == 0 && *len > 0)
		ft_copy_left(index, len);
	else if (ft_strcmp(HARD_OPTION_RIGHT, input) == 0 && *len > 0)
		ft_copy_right(index, len);
	else if (ft_strcmp(HARD_OPTION_C_LEFT, input) == 0 && *len > 0)
		ft_cut_left(index, len);
	else if (ft_strcmp(HARD_OPTION_C_RIGHT, input) == 0 && *len > 0\
	&& *index < *len)
		ft_cut_right(index, len);
	else if (ft_strcmp(HARD_OPTION_P, input) == 0)
		ft_paste_at(index, len);
	else
		ft_setterm("bl");
	return (0);
}

static int	ft_history(char *input, int *index, int *len)
{
	t_cursor	*cr;
	t_hist		**head;

	cr = get_cursor();
	head = get_history();
	if (ft_strcmp(HARD_UP, input) == 0)
	{
		ft_dprintf(3, "index:%d\n", cr->index_hist);
		if (cr->index_hist == -1 && *get_listd())
			ft_save_cmd();
		ft_get_history(NEXT, cr, index, len);
	}
	else if (ft_strcmp(HARD_DOWN, input) == 0)
	{
		ft_dprintf(3, "index:%d\n", cr->index_hist);
		if (cr->index_hist == 0)
			ft_reset_cmd(index, len, cr);
		else
			ft_get_history(PREV, cr, index, len);
	}
	else
		return (ft_clipboard(input, index, len));
	return (0);
}

static int	ft_key2(char *input, int *index, int *len, t_cursor *cr)
{
	int		i;

	i = -1;
	if (ft_strcmp(HARD_END, input) == 0 && *len != 0 && *index != *len)
	{
		while (++i < *len - *index)
			ft_move_cursor(RIGHT);
		*index = *len;
	}
	else if (ft_strcmp(HARD_SHIFT_PAGE_UP, input) == 0 && cr->line > 0)
		ft_move_cursor_multi(UP, index, len);
	else if (ft_strcmp(HARD_SHIFT_PAGE_DOWN, input) == 0
	&& cr->line < *len / cr->row_with)
		ft_move_cursor_multi(DOWN, index, len);
	else if (ft_strcmp(HARD_SHIFT_LEFT, input) == 0)
		ft_step_word(LEFT, index, len);
	else if (ft_strcmp(HARD_SHIFT_RIGHT, input) == 0)
		ft_step_word(RIGHT, index, len);
	else
		return (ft_history(input, index, len));
	return (0);
}

int			ft_key(char *input, int *index, int *len)
{
	t_keystrock	*strock;
	t_cursor	*cursor;
	int			i;

	i = -1;
	cursor = get_cursor();
	strock = get_keystrock();
	if (ft_strcmp(strock->left_a, input) == 0\
	&& *index > 0 && (*index = *index - 1) != -2)
		ft_move_cursor(LEFT);
	else if (ft_strcmp(strock->right_a, input) == 0\
	&& *index < *len && (*index = *index + 1) != -2)
		ft_move_cursor(RIGHT);
	else if (ft_strcmp(strock->home, input) == 0 && *index != 0 && *len != 0)
	{
		while (++i < *index)
			ft_move_cursor(LEFT);
		*index = 0;
	}
	else
		return (ft_key2(input, index, len, cursor));
	return (0);
}
