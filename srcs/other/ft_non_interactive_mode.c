/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_non_interactive_mode.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 14:02:50 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 09:36:48 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int		ft_read_opt(int ac, char **av, char **ev)
{
	int	i;

	i = 1;
	if (ft_strcmp(av[i], "-t") == 0)
		i = 2;
	if (i >= ac - 1)
	{
		if (i < ac && ft_strcmp(av[i], "hash") == 0)
			return (ft_test_hash(ev));
		return (!!ft_dprintf(2, "21sh : error missing argument\n"));
	}
	if (ac > 4)
		return (!!ft_dprintf(2, "21sh : error too much argument\n"));
	if (ft_strcmp(av[i], "lexer") == 0)
		return (ft_test_lexer(av[i + 1]));
	if (ft_strcmp(av[i], "parser") == 0)
		return (ft_test_parser(av[i + 1]));
	if (ft_strcmp(av[i], "argv") == 0)
		return (ft_test_argv(av[i + 1], ev));
	ft_dprintf(2, "21sh : %s : unknow argument\n", av[i]);
	return (0);
}

int		ft_read_argv(int ac, char **av, char **ev)
{
	t_fd	fd;
	char	*line;
	char	*tmp;
	int		res;

	if (ac > 2 || ft_strcmp(av[1], "-t") == 0)
		return (ft_read_opt(ac, av, ev));
	if (ft_access(av[1], R_OK) || ((fd = open(av[1], O_RDONLY)) < 0\
	&& (g_errn = ERR_OPEN)))
		return (!!ft_warn2_errn("21sh", av[1], g_errn));
	if (!(*get_env() = ft_env_tab_list(ev)) || dup2(fd, STDIN_FILENO) < 0)
		f_exit(0, 0, 0);
	ft_set_signal();
	ft_set_env();
	while ((res = get_next_line(fd, &line)) > 0)
	{
		if (!(tmp = ft_strjoin(line, "\n")))
			exit(EXIT_FAILURE);
		free(line);
		ft_evalcmd(tmp);
	}
	ft_env_free_all(get_env());
	if (res == -1)
		exit(ft_dprintf(2, "21sh : error gnl\n"));
	return (*get_res());
}

int		ft_non_interactive_mode(int ac, char **av, char **ev)
{
	char	*line;
	char	*tmp;
	int		res;

	*get_is_insteract() = 0;
	if (ac > 1)
		return (ft_read_argv(ac, av, ev));
	else
	{
		if (!(*get_env() = ft_env_tab_list(ev)))
			f_exit(0, 0, 0);
		ft_set_signal();
		ft_set_env();
		while ((res = get_next_line(STDIN_FILENO, &line)) > 0)
		{
			if (!(tmp = ft_strjoin(line, "\n")))
				exit(EXIT_FAILURE);
			free(line);
			ft_evalcmd(tmp);
		}
		ft_env_free_all(get_env());
		if (res == -1)
			exit(ft_dprintf(2, "21sh : error gnl\n"));
		return (*get_res());
	}
}
