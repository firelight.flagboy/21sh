/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 15:19:48 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:27:47 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HASH_H
# define HASH_H

# include "ft_21sh_o_include.h"
# include "environ.h"

# ifndef HASH_SIZE
#  define HASH_SIZE 10000
# endif

typedef struct		s_hash
{
	char			*key;
	char			*path;
	struct s_hash	*next;
}					t_hash;

t_hash				**get_hash(void);
void				ft_refresh_hash(void);
uintmax_t			ft_hashing(char *s);
void				ft_hash_free(t_hash **head);
void				ft_hash_node_free(t_hash **head);
t_hash				*ft_hash_create(char *key, char *path);
#endif
