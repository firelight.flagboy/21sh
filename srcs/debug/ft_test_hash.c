/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_test_hash.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 13:57:52 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:17:57 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "debug.h"

int		ft_test_hash(char **ev)
{
	t_hash	**hash;
	t_hash	*node;
	int		i;

	if (!(*get_env() = ft_env_tab_list(ev)))
		exit(EXIT_FAILURE);
	ft_refresh_hash();
	hash = get_hash();
	i = -1;
	while (++i < HASH_SIZE)
		if (hash[i])
		{
			ft_printf("hash[%d]:\n", i);
			node = hash[i];
			while (node)
			{
				ft_printf("\tnode : key : %-20s : value : %s\n",\
				node->key, node->path);
				node = node->next;
			}
		}
	ft_env_free_all(get_env());
	return (0);
}
