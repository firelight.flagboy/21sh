/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:10:44 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/20 09:32:50 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"
#include <stdio.h>

char		*ft_listd_to_str_r(t_listd **headref)
{
	char	*res;
	t_listd	*node;
	size_t	i;
	size_t	len;

	len = ft_listd_len_list(*headref);
	if (!(res = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	res[len] = 0;
	i = 0;
	node = *headref;
	while (node->next)
		node = node->next;
	while (node)
	{
		res[i++] = node->c;
		node = node->prev;
	}
	return (res);
}

int			ft_listd_add_at(t_listd **headref, ssize_t index, char c)
{
	t_listd	*head;
	t_listd	*node;
	ssize_t	i;

	head = *headref;
	if (!(node = ft_listd_create_elem(c)))
		return (1);
	if (head == NULL || index == 0)
	{
		node->next = head;
		*headref = node;
		if (head)
			head->prev = node;
		return (0);
	}
	i = 0;
	while (++i < index && head->next != NULL)
		head = head->next;
	node->next = head->next;
	node->prev = head;
	if (head->next)
		head->next->prev = node;
	head->next = node;
	return (0);
}

void		ft_listd_free(t_listd **headref)
{
	t_listd	*node;
	t_listd	*tmp;

	if (headref == NULL || *headref == NULL)
		return ;
	node = *headref;
	while (node)
	{
		tmp = node;
		node = node->next;
		free(tmp);
	}
	*headref = NULL;
}
