/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   evalcmd.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 11:45:59 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:29:25 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EVALCMD_H
# define EVALCMD_H

# include "ft_21sh_o_include.h"
# include "environ.h"
# include "hash.h"
# include "argv.h"
# include "redir.h"
# include "other.h"
# include "builtin.h"
# include "lexer_parser.h"

# ifndef NB_FC_BUILT
#  define NB_FC_BUILT 6
# endif

# ifndef READ
#  define READ 0
# endif

# ifndef WRITE
#  define WRITE 1
# endif

extern pid_t	g_pid;

typedef int		t_fd;
typedef t_fd	t_pipe[2];
typedef struct	s_built
{
	char		*name;
	int			(*built)(int, char**, t_env**);
}				t_built;

t_built			*get_tbuilt(void);
void			ft_evalcmd(char *cmd);
int				ft_ev_ast(t_ast *ast, uint *count);
void			ft_exit_code(int *res, char *name);

/*
** eval of the command
*/
int				ft_pipe_fork(t_ast *ast, t_fd input, uint *count);
int				ft_exec_pipe(t_ast *ast, t_fd input, uint *count);
int				ft_pipe_cmd(t_ast *ast, uint *count);
int				ft_eval_hash(char **av, t_ast *ast, uint *count);
int				ft_eval_direct_cmd(char **av, t_ast *ast, uint *count);
int				ft_eval_built(char **argv, int *rt, t_ast *ast, uint *count);
#endif
