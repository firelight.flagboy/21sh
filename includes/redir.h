/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redir.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 09:34:59 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 12:44:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REDIR_H
# define REDIR_H

# include "ft_21sh_o_include.h"
# include "libft.h"
# include "argv.h"
# include "lexer_parser.h"
# include "other.h"
# include "chterm.h"
# include "getinput.h"
# include "ft_printf.h"

# ifndef CREATE_FLAGS
#  define CREATE_FLAGS S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH
# endif

# ifndef OP_FLAGS_W
#  define OP_FLAGS_W O_WRONLY | O_CREAT | O_TRUNC
# endif

int			ft_create_heredoc(char *hd_name, uint count);
int			ft_rm_heredoc(uint count);
char		*ft_count_to_str(uint count);
int			ft_redir(t_token *token, int size, uint *count);
int			ft_redir_agre(t_token last, t_token cur, t_token next);
#endif
