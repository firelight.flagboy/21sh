/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_termcap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 10:07:20 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 13:51:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int		ft_get_termcap(void)
{
	char		*res;
	t_keystrock	*strock;

	if ((res = tgetstr("ks", NULL)) == NULL)
	{
		ft_dprintf(2, "21sh : can't use termcap\n");
		return (-1);
	}
	tputs(res, 0, ft_putchar_ent);
	strock = get_keystrock();
	if (!(strock->left_a = tgetstr("kl", NULL)))
		return (-1);
	if (!(strock->right_a = tgetstr("kr", NULL)))
		return (-1);
	if (!(strock->down_a = tgetstr("kd", NULL)))
		return (-1);
	if (!(strock->upper_a = tgetstr("ku", NULL)))
		return (-1);
	if (!(strock->del = tgetstr("kD", NULL)))
		return (-1);
	if (!(strock->home = tgetstr("kh", NULL)))
		return (-1);
	return (0);
}
