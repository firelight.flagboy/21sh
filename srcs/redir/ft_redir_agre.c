/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_redir_agre.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 09:42:17 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 09:14:08 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"

static int	ft_redir_agre_out_end(t_token next, int old)
{
	char	*s;
	int		n;

	if (!(s = ft_get_arg(next)))
		return (ft_warnx_errn("21sh", (g_errn = ERR_MALLOC)));
	if ((n = open(s, OP_FLAGS_W, CREATE_FLAGS)) < 0)
	{
		ft_dprintf(2, "21sh : %s :  error open\n", s);
		free(s);
		return (1);
	}
	dup2(n, old);
	free(s);
	return (0);
}

static int	ft_redir_agre_out(t_token last, t_token next)
{
	int		old;
	int		n;

	old = STDOUT_FILENO;
	if (last.name == IO_NUMBER)
		old = last.value[0] - '0';
	if (ft_strcmp(next.value, "-") == 0)
		return (close(old));
	if (ft_isall_num(next.value))
	{
		if ((n = ft_atoi(next.value)) >= 0 && n <= 9)
			dup2(n, old);
		else
			return (!!ft_dprintf(2, "error bad file descriptor\n"));
	}
	else
		ft_redir_agre_out_end(next, old);
	return (0);
}

static int	ft_redir_agre_in_end(t_token next, int old)
{
	char	*s;
	int		new;

	if (!(s = ft_get_arg(next)))
		return (ft_warnx_errn("21sh", (g_errn = ERR_MALLOC)));
	if (ft_access(s, R_OK))
	{
		ft_warn2_errn("21sh", s, g_errn);
		free(s);
		return (1);
	}
	if ((new = open(s, O_RDONLY)) < 0)
	{
		ft_dprintf(2, "21sh : %s : error open\n", s);
		free(s);
		return (1);
	}
	dup2(new, old);
	free(s);
	return (0);
}

static int	ft_redir_agre_in(t_token last, t_token next)
{
	int		old;
	int		new;

	old = (last.name == IO_NUMBER) ? last.value[0] - '0' : STDIN_FILENO;
	if (ft_strcmp(next.value, "-") == 0)
		return (close(old));
	if (ft_isall_num(next.value))
	{
		if ((new = ft_atoi(next.value)) >= 0 && new <= 9)
			dup2(old, new);
		else
			return (!!ft_dprintf(2, "error bad file descriptor\n"));
	}
	else
		ft_redir_agre_in_end(next, old);
	return (0);
}

int			ft_redir_agre(t_token last, t_token cur, t_token next)
{
	if (cur.name == LESS_AND)
		return (ft_redir_agre_in(last, next));
	if (cur.name == GREAT_AND)
		return (ft_redir_agre_out(last, next));
	return (0);
}
