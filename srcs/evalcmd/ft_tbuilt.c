/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tbuilt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 09:33:23 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/09 09:01:10 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

static void	ft_init_tbuilt_name(t_built *t)
{
	t[0].name = "cd";
	t[1].name = "echo";
	t[2].name = "env";
	t[3].name = "exit";
	t[4].name = "setenv";
	t[5].name = "unsetenv";
	t[6].name = NULL;
}

static void	ft_init_tbuilt_fc(t_built *t)
{
	t[0].built = cd;
	t[1].built = echo;
	t[2].built = env;
	t[3].built = f_exit;
	t[4].built = ft_setenv_main;
	t[5].built = ft_unsetenv_main;
	t[6].built = NULL;
}

t_built		*get_tbuilt(void)
{
	static t_built tbuilt[NB_FC_BUILT + 1];

	if (tbuilt[0].name == NULL)
	{
		ft_init_tbuilt_name(tbuilt);
		ft_init_tbuilt_fc(tbuilt);
	}
	return (tbuilt);
}
