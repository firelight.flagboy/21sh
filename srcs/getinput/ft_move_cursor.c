/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_cursor.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:24:25 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 11:57:32 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static void	ft_cursor_resize(t_cursor *cursor)
{
	int	d;

	d = (cursor->line * cursor->row_with) + cursor->row;
	cursor->line = d / cursor->row_with;
	cursor->row = d % cursor->row_with;
}

static void	ft_move_cursor_right(t_cursor *cur)
{
	if (cur->line == 0 && cur->row >= cur->row_with)
	{
		cur->line = cur->line + 1;
		cur->row = 0;
	}
	else if (cur->row >= cur->row_with - 1 && (cur->line = cur->line + 1))
		cur->row = 0;
	else
		cur->row = cur->row + 1;
}

int			ft_move_cursor_multi(int move, int *index, int *len)
{
	t_cursor	*curs;
	int			d;

	curs = get_cursor();
	(void)len;
	if (move == UP)
	{
		curs->line = curs->line - 1;
		if (curs->line == 0 && *index < curs->row_with)
		{
			curs->row = *get_len_p();
			*index = 0;
		}
		else
			*index = (curs->line * curs->row_with) + curs->row - *get_len_p();
	}
	else if (move == DOWN)
	{
		curs->line = curs->line + 1;
		d = (curs->line * curs->row_with) + curs->row;
		if (curs->row > *len % curs->row_with + *get_len_p())
			curs->row = *len % curs->row_with + *get_len_p();
		*index = (curs->line * curs->row_with) + curs->row - *get_len_p();
	}
	return (1);
}

int			ft_move_cursor(int move)
{
	t_cursor *cur;

	cur = get_cursor();
	if (move == NOP)
		ft_cursor_resize(cur);
	else if (move == LEFT)
	{
		if (cur->row == 0)
		{
			if (cur->line > 0)
			{
				cur->line = cur->line - 1;
				cur->row = cur->row_with - 1;
			}
		}
		else
			cur->row = cur->row - 1;
	}
	else if (move == RIGHT)
		ft_move_cursor_right(cur);
	return (1);
}
