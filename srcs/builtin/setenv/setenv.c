/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 13:51:09 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:34:52 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int		ft_setenv_main(int argc, char **argv, t_env **env)
{
	t_env *node;

	if (argc < 3)
	{
		return (!!ft_dprintf(2, "usage :%s [key] [value]\n%s: %s\n",\
		argv[0], argv[0], "missing argument"));
	}
	else if (argc > 3)
	{
		return (!!ft_dprintf(2, "usage :%s [key] [value]\n%s: %s\n",\
		argv[0], argv[0], "too much arguments"));
	}
	if (ft_strchr(argv[1], '=') || ft_strchr(argv[2], '='))
		return (ft_warnx(argv[0], "argument must not have '=' in it"));
	ft_setenv(env, argv[1], argv[2]);
	if (ft_strcmp(argv[1], "PATH") == 0)
		ft_refresh_hash();
	node = *env;
	while (node)
	{
		ft_printf("%s=%s\n", node->key, node->value);
		node = node->next;
	}
	return (0);
}
