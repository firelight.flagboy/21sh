/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_uselink.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 13:31:26 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 10:04:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

int		ft_uselink(char ***argv)
{
	char	**av;
	int		res;
	int		p;

	av = *argv;
	res = 2;
	while ((*av) && ((*av)[0] == '-' && (*av)[1] != 0))
	{
		if ((p = 0) != 1 && (*av)[1] == '-'\
		&& (*av)[2] == 0 && (av = av + 1))
			break ;
		while ((*av)[++p])
		{
			if ((*av)[p] == 'P')
				res = 0;
			else if ((*av)[p] == 'L')
				res = 2;
			else
				return (ft_bad_option("cd", (*av)[p]));
		}
		av++;
	}
	*argv = av;
	return (res);
}
