/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_redir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 14:28:40 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:57:26 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"

static int	ft_free(char *s)
{
	free(s);
	return (1);
}

static int	ft_redir_input_end(char *str, int old)
{
	int		new;

	if ((new = open(str, O_RDONLY)) < 0)
	{
		ft_dprintf(2, "21sh : %s :open error\n", str);
		ft_free(str);
		return (1);
	}
	dup2(new, old);
	close(new);
	ft_free(str);
	return (0);
}

static int	ft_redir_input(t_token last, t_token cur, t_token next, uint *count)
{
	int		old;
	char	*str;
	char	s[25];

	old = (last.name == IO_NUMBER) ? last.value[0] - '0' : STDIN_FILENO;
	if (cur.name == DLESS)
	{
		ft_strcpy(s, "/tmp/heredoc-");
		ft_strcat(s, ft_count_to_str(*count));
		s[24] = 0;
		str = ft_strdup(s);
		*count = *count + 1;
	}
	else
	{
		if (!(str = ft_get_arg(next)))
			return (ft_warnx_errn("21sh", (g_errn = ERR_MALLOC)));
		if (ft_access(str, R_OK))
		{
			ft_warn2_errn("21sh", str, g_errn);
			ft_free(str);
			return (1);
		}
	}
	return (ft_redir_input_end(str, old));
}

static int	ft_redir_output(t_token last, t_token cur, t_token next)
{
	int		old;
	int		new;
	int		opf;
	char	*str;

	old = STDOUT_FILENO;
	if (last.name == IO_NUMBER)
		old = last.value[0] - '0';
	opf = O_APPEND;
	if (cur.name == GREAT)
		opf = O_TRUNC;
	if (!(str = ft_get_arg(next)))
		return (ft_warnx_errn("21sh", (g_errn = ERR_MALLOC)));
	if ((new = open(str, opf | O_WRONLY | O_CREAT, CREATE_FLAGS)) < 0)
	{
		ft_dprintf(2, "21sh : %s : error open\n", str);
		free(str);
		return (1);
	}
	free(str);
	if (dup2(new, old) == -1)
		ft_dprintf(2, "21sh : error dup\n");
	close(new);
	return (0);
}

int			ft_redir(t_token *token, int size, uint *count)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (token[i].family == OP_REDIR)
		{
			if (token[i].name == LESS || token[i].name == DLESS)
			{
				if (ft_redir_input(token[i - 1], token[i], token[i + 1], count))
					return (1);
			}
			else if (token[i].name == GREAT || token[i].name == DGREAT)
			{
				if (ft_redir_output(token[i - 1], token[i], token[i + 1]))
					return (1);
			}
			else if (token[i].name == LESS_AND || token[i].name == GREAT_AND)
				if (ft_redir_agre(token[i - 1], token[i], token[i + 1]))
					return (1);
		}
		i++;
	}
	return (0);
}
