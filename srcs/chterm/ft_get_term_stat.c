/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_term_stat.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 09:56:28 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 13:53:51 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int		ft_get_term_stat(void)
{
	if (tcgetattr(0, get_term()) == -1 || tcgetattr(0, get_term_off()) == -1)
	{
		ft_dprintf(2, "21sh : can't get stat of terminal\n");
		return (-1);
	}
	if (ft_change_term_stat() == -1)
		return (-1);
	if (ft_get_termcap() == -1)
		return (-1);
	return (0);
}
