/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_test_parser.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 15:08:57 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:00:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "debug.h"

static void	ft_dysplay_node(t_token *tk, int size, int space)
{
	int		p;
	int		i;

	p = 0;
	while (++p < size)
	{
		i = 10 - 1;
		ft_printf("%*s", space - 10, "");
		ft_printf("\t-> value : %s\n", (tk[p].value[0] == '\n')\
		? "newline" : tk[p].value);
	}
}

static void	ft_dysplay_ast(t_ast *node, int space)
{
	int		i;
	int		step;

	step = 10;
	if (node == NULL)
		return ;
	space += step;
	ft_dysplay_ast(node->left_son, space);
	if (node->left_son)
		ft_printf("\n");
	i = step - 1;
	ft_printf("%*s", space - 10, "");
	ft_printf("value : %s\n", (node->node_tokens[0].value[0] == '\n')\
	? "newline" : node->node_tokens[0].value);
	ft_dysplay_node(node->node_tokens, node->size_tab, space);
	if (node->right_son)
		ft_printf("\n");
	ft_dysplay_ast(node->right_son, space);
}

static int	ft_process_test_parser(char *s)
{
	t_ast *ast;

	if (!(ast = ft_get_ast(s)))
		exit(EXIT_FAILURE);
	ft_dysplay_ast(ast, 0);
	ft_delete_ast(ast);
	return (0);
}

int			ft_test_parser(char *s)
{
	size_t	l;
	size_t	fl;
	int		addnl;
	char	*tmp;

	l = ft_strlen(s);
	addnl = 0;
	if (s[l - 1] != '\n')
	{
		addnl = 1;
		fl = l + 1;
	}
	else
		fl = l;
	if (!(tmp = (char*)malloc((fl + 1) * sizeof(char))))
		exit(EXIT_FAILURE);
	ft_strcpy(tmp, s);
	if (addnl)
		ft_strcat(tmp, "\n");
	return (ft_process_test_parser(tmp));
}
