/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_copy_right.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/20 13:19:16 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/23 09:47:28 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int		ft_copy_right(int *index, int *len)
{
	char	**clipboard;
	t_listd *node;
	int		i;

	clipboard = get_clipboard();
	node = *get_listd();
	if ((*clipboard))
		free((*clipboard));
	if (!((*clipboard) = (char*)malloc((*len - *index + 1) * sizeof(char))))
		return (ft_warnx_errn("clipboard manager", (g_errn = ERR_MALLOC)));
	i = -1;
	while (++i < *index && node)
		node = node->next;
	i = -1;
	while (node)
	{
		(*clipboard)[++i] = node->c;
		node = node->next;
	}
	(*clipboard)[++i] = 0;
	return (0);
}
