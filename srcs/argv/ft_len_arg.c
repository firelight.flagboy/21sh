/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_len_arg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 14:47:15 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 09:06:42 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "evalcmd.h"

static int	ft_len_env(char **ps)
{
	char	*s;
	int		i;
	char	env[2048];

	s = *ps;
	i = 1;
	if (*s == '~')
		ft_strcpy(env, "HOME");
	else
	{
		i = ft_get_env_name(env, s);
		env[i - 1] = 0;
	}
	*ps = s + i;
	if (!(s = ft_getenv(get_env(), env)))
		return (0);
	i = 0;
	while (s[i])
		i++;
	return (i);
}

int			ft_len_arg(char *arg)
{
	int		res;
	char	type;
	char	*t;

	res = 0;
	type = 0;
	t = 0;
	while (*arg)
	{
		if (type == 0 && (*arg == '\'' || *arg == '"') && (t = arg))
			type = *arg;
		if (type != '\'' &&\
		(*arg == '$' || (*arg == '~' && (arg[1] == '/' || arg[1] == 0))))
		{
			res += ft_len_env(&arg);
			continue;
		}
		if (*arg != type)
			res++;
		else if (t != arg)
			type = 0;
		arg++;
	}
	return (res);
}
