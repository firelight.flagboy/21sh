/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_step_word.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 14:12:50 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/23 09:48:44 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static void	ft_move_left(int *index, t_listd **headref)
{
	t_listd *node;
	int		i;

	node = *headref;
	i = -1;
	while (++i < *index && node)
		node = node->next;
	if (node && !ft_isspace(node->c))
		while (node && !ft_isspace(node->c) && *index > 0)
		{
			node = node->prev;
			ft_move_cursor(LEFT);
			*index = *index - 1;
		}
	while (node && ft_isspace(node->c) && *index > 0 && (*index = *index - 1))
	{
		node = node->prev;
		ft_move_cursor(LEFT);
	}
	while (node && node->prev && !ft_isspace(node->prev->c) && *index > 0)
	{
		node = node->prev;
		ft_move_cursor(LEFT);
		*index = *index - 1;
	}
}

static void	ft_move_right(int *index, t_listd **headref)
{
	t_listd *node;
	int		i;

	node = *headref;
	i = -1;
	while (++i < *index && node)
		node = node->next;
	if (node && !ft_isspace(node->c))
		while (node && !ft_isspace(node->c))
		{
			node = node->next;
			ft_move_cursor(RIGHT);
			*index = *index + 1;
		}
	while (node && ft_isspace(node->c))
	{
		node = node->next;
		ft_move_cursor(RIGHT);
		*index = *index + 1;
	}
}

int			ft_step_word(int move, int *index, int *len)
{
	t_listd			**headref;
	t_listd			*node;
	static t_listd	nut;

	headref = get_listd();
	if (*headref == NULL)
		return (1);
	if (move == LEFT && *index > 0)
	{
		nut.c = ' ';
		node = *headref;
		while (node->next)
			node = node->next;
		node->next = &nut;
		nut.prev = node;
		ft_move_left(index, headref);
		nut.prev->next = NULL;
	}
	if (move == RIGHT && *index < *len)
		ft_move_right(index, headref);
	return (1);
}
