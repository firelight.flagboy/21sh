/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/27 08:13:54 by eduriez           #+#    #+#             */
/*   Updated: 2017/11/30 17:09:21 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "get_next_line.h"
#include "libft.h"

static	int		ft_pick_endl_in_buffer(char **line, char *buf, int endl_index)
{
	char	*tmp;

	tmp = ft_strsub(buf, 0, endl_index);
	if (tmp == NULL)
		return (-1);
	*line = (char*)ft_memrealloc(*line, tmp);
	ft_memdel((void**)&tmp);
	if (*line == NULL)
		return (-1);
	if (buf[endl_index + 1] != '\0')
	{
		tmp = ft_strdup(buf + endl_index + 1);
		ft_bzero(buf, BUFF_SIZE);
		if (tmp == NULL)
		{
			ft_memdel((void**)line);
			return (-1);
		}
		ft_strcpy(buf, tmp);
		ft_memdel((void**)&tmp);
	}
	else
		ft_bzero(buf, BUFF_SIZE);
	return (1);
}

static	int		ft_read_new_line(const int fd, char *buf, char **line)
{
	int	endl_index;
	int	ret;

	ret = read(fd, buf, BUFF_SIZE);
	endl_index = ft_strchr_index(buf, '\n');
	while (ret != -1 && ret != 0 && endl_index == -1)
	{
		*line = (char*)ft_memrealloc(*line, buf);
		if (*line == NULL)
			ret = -1;
		ft_bzero(buf, BUFF_SIZE);
		ret = read(fd, buf, BUFF_SIZE);
		endl_index = ft_strchr_index(buf, '\n');
	}
	if (ret == 0)
	{
		if (*line != NULL)
			return (1);
		return (0);
	}
	else if (ret == -1)
		return (ret);
	return (ft_pick_endl_in_buffer(line, buf, endl_index));
}

int				get_next_line(const int fd, char **line)
{
	static	char	buf[BUFF_SIZE + 1] = {0};
	int				endl_index;

	if (line == NULL)
		return (-1);
	*line = NULL;
	if (ft_str_is_empty(buf))
		return (ft_read_new_line(fd, buf, line));
	else
	{
		endl_index = ft_strchr_index(buf, '\n');
		if (endl_index == -1)
		{
			*line = ft_strdup(buf);
			if (*line == NULL)
				return (-1);
			ft_bzero(buf, BUFF_SIZE);
			return (ft_read_new_line(fd, buf, line));
		}
		return (ft_pick_endl_in_buffer(line, buf, endl_index));
	}
}
