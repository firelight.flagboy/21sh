/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_test_argv.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/10 15:08:57 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:17:28 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "debug.h"

static void	ft_dysplay_av(char **av, int node)
{
	int		i;

	i = -1;
	ft_printf("node : %d : %s\n", node, av[0]);
	while (av[++i])
		ft_printf("\t[%-3d]:[%s]\n", i, av[i]);
	ft_free_tchar(av);
}

static void	ft_dysplay_argv(t_ast *ast, int node)
{
	char	**av;
	t_ast	*no;

	if (ast == NULL)
		return ;
	if (ast->left_son == NULL)
	{
		if ((ft_get_argv(&av, ast->node_tokens, ast->size_tab)) < 0)
			exit(!!ft_dprintf(2, "21sh : error malloc"));
		ft_dysplay_av(av, node);
	}
	else
	{
		no = ast->left_son;
		if ((ft_get_argv(&av, no->node_tokens, no->size_tab)) < 0)
			exit(!!ft_dprintf(2, "21sh : error malloc"));
		ft_dysplay_av(av, node);
		ft_dysplay_argv(ast->right_son, node + 1);
	}
}

static int	ft_process_test_argv(char *s)
{
	t_ast *ast;

	if (!(ast = ft_get_ast(s)))
		exit(EXIT_FAILURE);
	ft_dysplay_argv(ast, 0);
	ft_env_free_all(get_env());
	return (0);
}

int			ft_test_argv(char *s, char **ev)
{
	size_t	l;
	size_t	fl;
	int		addnl;
	char	*tmp;

	if (!(*get_env() = ft_env_tab_list(ev)))
		exit(EXIT_FAILURE);
	l = ft_strlen(s);
	addnl = 0;
	if (s[l - 1] != '\n')
	{
		addnl = 1;
		fl = l + 1;
	}
	else
		fl = l;
	if (!(tmp = (char*)malloc((fl + 1) * sizeof(char))))
		exit(EXIT_FAILURE);
	ft_strcpy(tmp, s);
	if (addnl)
		ft_strcat(tmp, "\n");
	return (ft_process_test_argv(tmp));
}
