# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    variable.mk                                        :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/10 10:25:50 by fbenneto          #+#    #+#              #
#    Updated: 2018/03/13 14:16:47 by fbenneto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#######
# LIB #
#######

LIB_PRINTF_DIR =\
	ft_printf/
LIB_PRINTF_NAME =\
	libftprintf.a

LIB_PRINTF = $(addprefix $(LIB_PRINTF_DIR), $(LIB_PRINTF_NAME))

LIBFT_DIR =\
	libft/
LIBFT_NAME =\
	libft.a

LIBFT = $(addprefix $(LIBFT_DIR), $(LIBFT_NAME))

##########
# HEADER #
##########

INC_DIR_LIBFT	=\
	$(LIBFT_DIR)includes/
INC_NAME_LIBFT	=\
	libft.h

INCLUDE_LIBFT	= $(addprefix $(INC_DIR_LIBFT), $(INC_NAME_LIBFT))

INC_LIBFT	= -I $(INC_DIR_LIBFT)

INC_DIR_PRINTF	=\
	$(LIB_PRINTF_DIR)includes/
INC_NAME_PRINTF	=\
	ft_printf_typedef.h\
	ft_printf_header_fc.h\
	ft_printf_d.h\
	ft_printf.h\

INCLUDE_PRINTF	= $(addprefix $(INC_DIR_PRINTF), $(INC_NAME_PRINTF))

INC_PRINTF	= -I $(INC_DIR_PRINTF)

INC_DIR =\
	./includes/

INC_NAME = \
	redir.h\
	other.h\
	builtin.h\
	environ.h\
	evalcmd.h\
	ft_21sh_define_and_other.h\
	ft_21sh_o_include.h\
	ft_21sh.h\
	ft_listd.h\
	hash.h\
	history.h\
	lexer_parser.h\
	my_errno.h\

INCLUDE	= $(addprefix $(INC_DIR), $(INC_NAME))
INC	= -I $(INC_DIR)

#############
# SRC_LIBFT #
#############

SRCS_LIBFT_NAME = $(shell make -C $(LIBFT_DIR) print_src)
SRCS_LIBFT = $(addprefix $(LIBFT_DIR), $(SRCS_LIBFT_NAME))

##############
# SRC_PRINTF #
##############

SRCS_PRINTF_NAME = $(shell make -C $(LIB_PRINTF_DIR) print_src)
SRCS_PRINTF = $(addprefix $(LIB_PRINTF_DIR), $(SRCS_PRINTF_NAME))

#######
# SRC #
#######

SRC_DIR	= ./srcs/

SRC_CHTERM_DIR= chterm/

SRC_CHTERM_NAME=\
	ft_change_term_stat.c\
	ft_get_term_stat.c\
	ft_get_termcap.c\
	ft_reset_term.c\
	ft_setterm.c\

SRC_CHTERM = $(addprefix $(SRC_CHTERM_DIR), $(SRC_CHTERM_NAME))

SRC_ENVIRON_DIR= environ/

SRC_ENVIRON_NAME=\
	ft_list_env.c\
	ft_setenv.c\
	ft_list_env_2.c\
	ft_getenv.c\

SRC_ENVIRON = $(addprefix $(SRC_ENVIRON_DIR), $(SRC_ENVIRON_NAME))

SRC_FT_LISTD_DIR= ft_listd/

SRC_FT_LISTD_NAME=\
	ft_listd.c\
	ft_listd_2.c\
	ft_listd_3.c\

SRC_FT_LISTD = $(addprefix $(SRC_FT_LISTD_DIR), $(SRC_FT_LISTD_NAME))

SRC_CLIPBOARD_DIR= clipboard/

SRC_CLIPBOARD_NAME=\
	ft_copy_left.c\
	ft_copy_right.c\
	ft_cut_left.c\
	ft_cut_right.c\
	ft_paste_at.c\

SRC_CLIPBOARD = $(addprefix $(SRC_CLIPBOARD_DIR), $(SRC_CLIPBOARD_NAME))

SRC_ARGV_DIR=\
	argv/

SRC_ARGV_NAME=\
	ft_get_arg.c\
	ft_get_argv.c\
	ft_len_arg.c\
	ft_fill_arg.c\

SRC_ARGV = $(addprefix $(SRC_ARGV_DIR), $(SRC_ARGV_NAME))

SRC_EVALCMD_DIR=\
	evalcmd/

SRC_EVALCMD_NAME=\
	ft_tbuilt.c\
	ft_pipe.c\
	ft_evalcmd.c\
	ft_eval_ast.c\
	ft_eval_built.c\
	ft_eval_hash.c\
	ft_eval_direct_cmd.c\

SRC_EVALCMD = $(addprefix $(SRC_EVALCMD_DIR), $(SRC_EVALCMD_NAME))

SRC_REDIR_DIR=\
	redir/

SRC_REDIR_NAME=\
	heredoc.c\
	ft_redir.c\
	ft_redir_agre.c\
	ft_rm_heredoc.c\
	ft_count_to_str.c\

SRC_REDIR = $(addprefix $(SRC_REDIR_DIR), $(SRC_REDIR_NAME))

SRC_DEBUG_DIR=\
	debug/

SRC_DEBUG_NAME=\
	ft_token_to_str.c\
	ft_test_argv.c\
	ft_test_hash.c\
	ft_test_lexer.c\
	ft_test_parser.c\

SRC_DEBUG = $(addprefix $(SRC_DEBUG_DIR), $(SRC_DEBUG_NAME))

SRC_OTHER_DIR=\
	other/

SRC_OTHER_NAME=\
	ft_non_interactive_mode.c\
	singleton.c\
	singleton2.c\
	signal.c\
	ft_access.c\

SRC_OTHER = $(addprefix $(SRC_OTHER_DIR), $(SRC_OTHER_NAME))

SRC_HISTORY_DIR=\
	history/

SRC_HISTORY_NAME=\
	ft_hist_nav.c\
	ft_t_hist.c\

SRC_HISTORY = $(addprefix $(SRC_HISTORY_DIR), $(SRC_HISTORY_NAME))

SRC_GETINPUT_DIR= getinput/

SRC_GETINPUT_NAME=\
	ft_save_cmd.c\
	ft_display.c\
	ft_key.c\
	ft_step_word.c\
	ft_move_cursor.c\
	ft_process.c\
	ft_read.c\

SRC_GETINPUT = $(addprefix $(SRC_GETINPUT_DIR), $(SRC_GETINPUT_NAME))

SRC_ERRNO_DIR = errno/

SRC_ERRNO_NAME =\
	my_errno.c\
	my_errno_arg.c\
	my_errno_str.c\
	my_errno_atoi.c\
	my_errno_error.c\
	my_errno_warnx.c\
	my_errno_ft_strerror.c\

SRC_ERRNO = $(addprefix $(SRC_ERRNO_DIR), $(SRC_ERRNO_NAME))

SRC_LEXER_PARSER_DIR = lexer_parser/

SRC_LEXER_PARSER_NAME=\
	ast.c\
	cmd_user_to_ast.c\
	control_operators.c\
	frees.c\
	ft_get_leaf_content.c\
	ft_get_lex_word_function.c\
	ft_is_valid_cmd.c\
	lex_quotes.c\
	lexer.c\
	redirections_operators.c\
	tests_functions.c\
	tokens.c\

SRC_LEXER_PARSER = $(addprefix $(SRC_LEXER_PARSER_DIR), $(SRC_LEXER_PARSER_NAME))

SRC_BUILT_DIR =\
	builtin/

SRC_ECHO_DIR =\
	$(SRC_BUILT_DIR)echo/
SRC_ECHO_NAME =\
	echo.c

SRC_ECHO = $(addprefix $(SRC_ECHO_DIR), $(SRC_ECHO_NAME))

SRC_ENV_DIR =\
	$(SRC_BUILT_DIR)env/
SRC_ENV_NAME =\
	env.c\
	ft_isbuilt.c\
	ft_env_call_prog.c\

SRC_ENV = $(addprefix $(SRC_ENV_DIR), $(SRC_ENV_NAME))

SRC_SETENV_DIR =\
	$(SRC_BUILT_DIR)setenv/
SRC_SETENV_NAME =\
	setenv.c

SRC_SETENV = $(addprefix $(SRC_SETENV_DIR), $(SRC_SETENV_NAME))

SRC_UNSETENV_DIR =\
	$(SRC_BUILT_DIR)unsetenv/
SRC_UNSETENV_NAME =\
	unsetenv.c

SRC_UNSETENV = $(addprefix $(SRC_UNSETENV_DIR), $(SRC_UNSETENV_NAME))

SRC_EXIT_DIR =\
	$(SRC_BUILT_DIR)exit/
SRC_EXIT_NAME =\
	exit.c

SRC_EXIT = $(addprefix $(SRC_EXIT_DIR), $(SRC_EXIT_NAME))

SRC_CD_DIR =\
	$(SRC_BUILT_DIR)cd/
SRC_CD_NAME =\
	cd.c\
	ft_path_normalize.c\
	ft_move_to_abs.c\
	ft_trim_backslash.c\
	ft_move_home.c\
	ft_move_to.c\
	ft_move_back.c\
	ft_check_file.c\
	ft_check_dir.c\
	ft_uselink.c\

SRC_CD = $(addprefix $(SRC_CD_DIR), $(SRC_CD_NAME))

SRC_HASH_DIR =\
	hash/
SRC_HASH_NAME =\
	ft_hash.c\
	ft_refresh_hash.c\
	get_hash.c\

SRC_HASH = $(addprefix $(SRC_HASH_DIR), $(SRC_HASH_NAME))

SRC_DIR_ALL=\
	$(SRC_BUILT_DIR)exit/\
	$(SRC_BUILT_DIR)cd/\
	$(SRC_BUILT_DIR)unsetenv/\
	$(SRC_BUILT_DIR)setenv/\
	$(SRC_BUILT_DIR)env/\
	$(SRC_BUILT_DIR)echo/\
	$(SRC_ARGV_DIR)\
	$(SRC_EVALCMD_DIR)\
	$(SRC_DEBUG_DIR)\
	$(SRC_ERRNO_DIR)\
	$(SRC_GETINPUT_DIR)\
	$(SRC_OTHER_DIR)\
	$(SRC_REDIR_DIR)\
	$(SRC_CLIPBOARD_DIR)\
	$(SRC_FT_LISTD_DIR)\
	$(SRC_HASH_DIR)\
	$(SRC_ENVIRON_DIR)\
	$(SRC_CHTERM_DIR)\
	$(SRC_HISTORY_DIR)\
	$(SRC_LEXER_PARSER_DIR)\

SRC_NAME =\
	$(SRC_DEBUG)\
	$(SRC_ARGV)\
	$(SRC_REDIR)\
	$(SRC_HISTORY)\
	$(SRC_ENVIRON)\
	$(SRC_ERRNO)\
	$(SRC_OTHER)\
	$(SRC_EXIT)\
	$(SRC_CD)\
	$(SRC_ECHO)\
	$(SRC_HASH)\
	$(SRC_ENV)\
	$(SRC_SETENV)\
	$(SRC_UNSETENV)\
	$(SRC_CHTERM)\
	$(SRC_FT_LISTD)\
	$(SRC_GETINPUT)\
	$(SRC_CLIPBOARD)\
	$(SRC_LEXER_PARSER)\
	$(SRC_EVALCMD)\
	ft_21sh.c\

SRC_ALL		=  $(addprefix $(SRC_DIR), $(SRC_NAME))

#######
# OBJ #
#######

OBJ_DIR			= ./obj/

OBJ_NAME	= $(SRC_NAME:.c=.o)
OBJ_DIR_ALL = $(addprefix $(OBJ_DIR), $(SRC_DIR_ALL))
OBJ_ALL			= $(addprefix $(OBJ_DIR), $(OBJ_NAME))

#########
# MACRO #
#########

NC		= "\\033[0m"
RED		= "\\033[31m"
GREEN	= "\\033[32m"
YELLOW	= "\\033[33m"
BLUE	= "\\033[34m"
MAJENTA	= "\\033[35m"
CYAN	= "\\033[36m"
BOLD	= "\\033[1m"
CHEK	= "✓"
OK		= "$(GREEN)$(CHEK)$(NC)"

make_classical:
	@echo "should not be use as a normal makefile, use ./Makefile"
	@make --file=Makefile all
