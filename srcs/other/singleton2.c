/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   singleton2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 14:34:31 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:00:02 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

t_listd		**get_listd(void)
{
	static t_listd *head;

	return (&head);
}

char		**get_clipboard(void)
{
	static char *str;

	return (&str);
}

t_env		**get_env(void)
{
	static t_env *head;

	return (&head);
}

int			*get_is_insteract(void)
{
	static int	interact;

	return (&interact);
}

char		**get_save_cmd(void)
{
	static char *save;

	return (&save);
}
