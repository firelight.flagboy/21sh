/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tab_strs_len.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 10:47:58 by eduriez           #+#    #+#             */
/*   Updated: 2018/02/07 10:48:41 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int		ft_tab_strs_len(char **tab_strs)
{
	int	length;

	length = 0;
	while (tab_strs[length] != NULL)
		length++;
	return (length);
}
