/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/07 09:25:29 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:00:21 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OTHER_H
# define OTHER_H

# include "ft_21sh_define_and_other.h"
# include "ft_21sh_o_include.h"
# include "my_errno.h"
# include "ft_listd.h"

extern pid_t g_pid;

int			ft_access(char *path, int mode);
void		handler_pid(int sig);
void		handler_quit(int sig);
void		handler_prompt(int sig);
void		ft_set_signal(void);
/*
** Singleton
*/
char		**get_save_cmd(void);
char		**get_clipboard(void);
int			*get_res(void);
t_termios	*get_term(void);
t_termios	*get_term_off(void);
t_cursor	*get_cursor(void);
t_keystrock	*get_keystrock(void);
t_listd		**get_listd(void);
int			*get_is_insteract(void);
#endif
