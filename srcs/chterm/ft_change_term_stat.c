/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_change_term_stat.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 10:01:58 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 14:35:15 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int		ft_change_term_stat(void)
{
	t_termios	*term;

	term = get_term();
	term->c_lflag &= ~(ICANON | ECHO);
	term->c_cc[VMIN] = 1;
	term->c_cc[VTIME] = 0;
	if (isatty(0))
		if (tcsetattr(0, TCSADRAIN, term) == -1)
		{
			ft_dprintf(2, "21sh : can't set the modified"\
			" attribut of the terminal\n");
			return (-1);
		}
	return (0);
}
