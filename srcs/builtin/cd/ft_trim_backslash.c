/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trim_backslash.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 09:08:38 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 09:10:52 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

char	*ft_trim_backslash(char *path)
{
	int	i;

	i = 0;
	while (path[i])
		i++;
	if (i - 1 > 0 && path[i - 1] == '/')
		path[i - 1] = 0;
	return (path);
}

int		ft_end_slash(char *path)
{
	char	lc;

	lc = 0;
	while (*path)
	{
		lc = *path;
		path++;
	}
	return (lc == '/');
}
