/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listd.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/15 15:06:41 by fbenneto          #+#    #+#             */
/*   Updated: 2018/02/15 15:28:45 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_listd.h"

t_listd		*ft_listd_create_elem(char c)
{
	t_listd	*node;

	if (!(node = (t_listd*)malloc(sizeof(t_listd))))
		return (NULL);
	node->next = NULL;
	node->prev = NULL;
	node->c = c;
	return (node);
}

int			ft_listd_add_front(char c, t_listd **headref)
{
	t_listd	*node;
	t_listd	*tmp;

	if (!(node = ft_listd_create_elem(c)))
		return (1);
	if (*headref == NULL)
	{
		*headref = node;
	}
	else
	{
		tmp = *headref;
		node->next = tmp;
		tmp->prev = node;
		*headref = node;
	}
	return (0);
}

int			ft_listd_add_back(char c, t_listd **headref)
{
	t_listd	*node;
	t_listd	*tmp;

	if (!(node = ft_listd_create_elem(c)))
		return (1);
	if (*headref == NULL)
	{
		*headref = node;
	}
	else
	{
		tmp = *headref;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = node;
		node->prev = tmp;
	}
	return (0);
}

size_t		ft_listd_len_list(t_listd *head)
{
	size_t	l;

	l = 0;
	while (head)
	{
		head = head->next;
		l++;
	}
	return (l);
}

char		*ft_listd_to_str(t_listd **headref)
{
	char	*res;
	t_listd	*node;
	size_t	i;
	size_t	len;

	len = ft_listd_len_list(*headref);
	if (!(res = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	res[len] = 0;
	i = 0;
	node = *headref;
	while (node)
	{
		res[i++] = node->c;
		node = node->next;
	}
	return (res);
}
