/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_21sh.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 08:58:51 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 11:57:01 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_21SH_H
# define FT_21SH_H

# include "ft_21sh_o_include.h"
# include "my_errno.h"
# include "libft.h"
# include "ft_21sh_define_and_other.h"
# include "ft_listd.h"
# include "environ.h"
# include "getinput.h"
# include "chterm.h"
# include "hash.h"
# include "redir.h"
# include "builtin.h"
# include "other.h"
# include "history.h"
# include "lexer_parser.h"
# include "debug.h"
# include "evalcmd.h"

pid_t g_pid;
/*
** Clipboard
*/
int			*get_len_p(void);
int			ft_paste_at(int *index, int *len);
int			ft_copy_left(int *index, int *len);
int			ft_copy_right(int *index, int *len);
int			ft_cut_left(int *index, int *len);
int			ft_cut_right(int *index, int *len);
/*
** Signal
*/
void		resize(int sig);
void		ft_set_signal(void);
/*
** 21sh
*/
void		ft_set_env(void);
int			ft_non_interactive_mode(int ac, char **av, char **ev);
void		ft_place_cursor(t_cursor *cursor);
void		ft_head_promt(void);
#endif
