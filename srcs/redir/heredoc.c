/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/09 09:07:18 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 10:27:25 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "redir.h"

static void	handler_here(int sig)
{
	t_listd		**head;
	t_termios	*term;

	head = get_listd();
	term = get_term();
	ft_listd_free(head);
	*head = ft_str_to_listd(get_cursor()->exit);
	term->c_cc[VMIN] = 0;
	term->c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSADRAIN, term) == -1)
		return ;
	(void)sig;
}

static int	init_here(char *hd_name, uint count, char *s)
{
	int fd;

	ft_strcpy(s, "/tmp/heredoc-");
	ft_strcat(s, ft_count_to_str(count));
	s[24] = 0;
	get_cursor()->exit = hd_name;
	if ((fd = open(s, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR)) < 0)
		return (-1);
	signal(SIGINT, handler_here);
	return (fd);
}

static int	ft_heredoc_no_interact(char *hd_name, int fd)
{
	int		res;
	char	*line;

	while ((res = get_next_line(STDIN_FILENO, &line)) > 0)
	{
		if (ft_strcmp(line, hd_name) == 0)
		{
			ft_strdel(&line);
			break ;
		}
		ft_dprintf(fd, "%s\n", line);
		ft_strdel(&line);
	}
	close(fd);
	if (res < 0)
		return (!!ft_dprintf(2, "21sh : heredoc : error gnl\n"));
	if (line)
		ft_strdel(&line);
	return (0);
}

int			ft_create_heredoc(char *hd_name, uint count)
{
	int		fd;
	char	*res;
	char	s[25];

	fd = init_here(hd_name, count, s);
	if (*get_is_insteract() == 0)
		return (ft_heredoc_no_interact(hd_name, fd));
	while (1)
	{
		get_cursor()->index_hist = -1;
		if ((res = ft_read("> ")))
		{
			if (ft_strcmp(res, hd_name) == 0)
			{
				free(res);
				break ;
			}
			ft_dprintf(fd, "%s\n", res);
		}
		free(res);
	}
	signal(SIGINT, handler_prompt);
	ft_change_term_stat();
	close(fd);
	ft_dprintf(3, "here\n");
	return (0);
}
