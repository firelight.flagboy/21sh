/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 11:23:58 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/14 09:16:29 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static void	ft_rz(int count)
{
	int		i;

	i = -1;
	while (++i < count)
	{
		ft_setterm("up");
	}
	ft_setterm("cr");
	ft_setterm("sc");
}

void		ft_place_cursor(t_cursor *cursor)
{
	int		row;

	ft_setterm("rc");
	row = cursor->row;
	if (cursor->line != 0)
		tputs(tgoto(tgetstr("DO", 0), 0, cursor->line), 0, ft_putchar_ent);
	if (row != 0)
		tputs(tgoto(tgetstr("RI", 0), 0, row), 0, ft_putchar_ent);
}

static int	ft_print(char *s, t_cursor *cursor)
{
	int		count;

	count = 0;
	if ((int)ft_strlen(s) + *get_len_p() <= cursor->row_with)
		ft_dprintf(1, "%s", s);
	else
	{
		count = (ft_strlen(s) + *get_len_p()) / cursor->row_with;
		ft_dprintf(1, "%s", s);
	}
	return (count);
}

void		ft_dysplay(t_listd **headref, char *prompt)
{
	t_cursor	*cursor;
	char		*t;
	int			count;

	cursor = get_cursor();
	ft_setterm("rc");
	ft_setterm("cd");
	if (!(t = ft_listd_to_str(headref)))
		f_exit(0, 0, 0);
	ft_dprintf(2, "%s", prompt);
	count = ft_print(t, cursor);
	if (count)
		ft_rz(count);
	ft_place_cursor(cursor);
	free(t);
}
