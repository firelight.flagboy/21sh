/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   environ.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 12:49:35 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/10 16:30:00 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENVIRON_H
# define ENVIRON_H

# include "ft_21sh_o_include.h"
# include "libft.h"
# include "my_errno.h"

typedef struct		s_env
{
	char			*key;
	char			*value;
	struct s_env	*next;
}					t_env;

t_env				**get_env(void);
char				*ft_getenv(t_env **headref, char *key);
int					ft_setenv(t_env **headref, char *key, char *value);
/*
** ft_list_env.c
*/
void				ft_env_free_all(t_env **headref);
t_env				*ft_env_new(char *key, char *value);
void				ft_env_push(t_env **headref, char *key, char *value);
t_env				*ft_env_tab_list(char **env);
size_t				ft_env_len(t_env *node);
/*
** ft_list_env_2.c
*/
char				*ft_env_node_str(t_env *node);
char				**ft_env_list_tab(t_env **headref);
void				ft_env_free_one(t_env *node);
void				ft_env_remove_if(t_env **headref, char *key);
#endif
