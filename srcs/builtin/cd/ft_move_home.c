/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move_home.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 14:50:40 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/13 11:59:03 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"

static int	ft_end_home(t_env **ev, int uselnk, char *home)
{
	char	old[PATH_MAX];

	home = ft_path_normalize(home);
	if (uselnk == 0)
	{
		getcwd(old, PATH_MAX);
		ft_setenv(ev, "PWD", old);
	}
	else
		ft_setenv(ev, "PWD", home);
	free(home);
	return (0);
}

int			ft_move_home(t_env **ev, int uselnk)
{
	char	*home;
	char	*tmp;
	char	old[PATH_MAX];

	if (!(home = ft_getenv(ev, "HOME")))
		return (ft_warnx("cd", "HOME not set"));
	home = ft_trim_backslash(home);
	if (ft_check_file(home) || ft_check_dir(home))
		return (ft_warn2_errn("cd", home, g_errn));
	if (uselnk == 0 || !(tmp = ft_getenv(ev, "PWD")))
	{
		getcwd(old, PATH_MAX);
		ft_setenv(ev, "OLDPWD", old);
	}
	else
		ft_setenv(ev, "OLDPWD", tmp);
	chdir(home);
	return (ft_end_home(ev, uselnk, home));
}
