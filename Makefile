# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/27 12:36:31 by fbenneto          #+#    #+#              #
#    Updated: 2018/03/13 11:47:25 by eduriez          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

##########
# TARGET #
##########

NAME=21sh

#######
# NOT #
#######

ifneq ($(words $(MAKECMDGOALS)),1)
.DEFAULT_GOAL = all
%:
		@$(MAKE) $@ --no-print-directory -rRf $(firstword $(MAKEFILE_LIST))
else
ifndef ECHO
T := $(shell $(MAKE) $(MAKECMDGOALS) --no-print-directory \
		-nrRf $(firstword $(MAKEFILE_LIST)) \
		ECHO="COUNTTHIS" | grep -c "COUNTTHIS")

N := x
C = $(words $N)$(eval N := x $N)
ECHO = printf "\r\e[K[21sh:      %3s%%]" "`expr "\`expr $C '*' 100 / $T\`"`"
endif

####################
# INCLUDE VARIABLE #
####################

include ./variable.mk

############
# COMPILER #
############

CC =clang
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S), Linux)
	CFLAGS += -O3
endif
ifeq ($(UNAME_S), Darwin)
	CFLAGS += -Wall -Werror -Wextra
endif

#########
# RULES #
#########

all : $(NAME)

$(SRC_NAME) : $(INC_NAME)

$(NAME) : $(LIB_PRINTF) $(LIBFT) $(OBJ_DIR) $(OBJ_DIR_ALL) $(OBJ_ALL)
	@echo
	@$(ECHO)
	@printf "\tcompile binary $(BOLD)$(CYAN)$@ "
	@$(CC) $(CFLAGS) -ltermcap -o $@ $(OBJ_ALL) $(LIB_PRINTF) $(LIBFT)
	@printf $(NC)$(OK)'\n'

$(OBJ_DIR_ALL):
	@$(ECHO)
	@printf "\tcreating folder $(MAJENTA)$(BOLD)$@$(NC)"
	@mkdir -p $@
	@printf ' '$(OK)

$(OBJ_DIR) :
	@$(ECHO)
	@printf "\tcreating folder $(MAJENTA)$(BOLD)$@$(NC) "
	@mkdir -p $@
	@printf $(OK)'\n'

$(OBJ_DIR)%.o: $(SRC_DIR)%.c ./Makefile ./variable.mk
	@$(ECHO)
	@printf "\tcompile $(YELLOW)$(BOLD)$<$(NC)"
	@$(CC) $(CFLAGS) -o $@ -c $< $(INC) $(INC_PRINTF) $(INC_LIBFT)

$(SRC_ALL): $(INCLUDE) $(INCLUDE_PRINTF)

$(LIBFT) : $(SRCS_LIBFT)
	@make -C $(LIBFT_DIR) $(LIBFT_NAME)

$(LIB_PRINTF) : $(SRCS_PRINTF)
	@make -C $(LIB_PRINTF_DIR) $(LIB_PRINTF_NAME)

clean : $(OBJ_DIR)
	@make -C $(LIB_PRINTF_DIR) clean
	@make -C $(LIBFT_DIR) clean
	@$(ECHO)
	@printf "\trm all $(BOLD)$(RED)obj file"
	@rm -rf $(OBJ_DIR)
	@printf '\t'$(NC)$(OK)'\n'

norme : $(SRC_ALL) $(INCLUDE)
	@make -C $(LIB_PRINTF_DIR) norme
	@printf "[21sh: $@]\n"
	@norminette $^ | grep -B 1 "Warning\|Error" || true
	@printf "$(GREEN)$(BOLD)DONE$(NC)\n"

fclean : clean
	@make -C $(LIB_PRINTF_DIR) naelc
	@make -C $(LIBFT_DIR) fclean
	@$(ECHO)
	@printf "\trm $(BOLD)$(CYAN)$(NAME)"
	@rm -f $(NAME)
	@printf '\t'$(NC)$(OK)'\n'

jump :
	@printf "\n"

proper :
	@make -C ./ all
	@make -C ./ clean

re :
	@make -C ./ fclean
	@make -C ./ all

run :
	@make -C ./ all
	./21sh || true

.PHONY: proper re norme all fclean clean naelc run jump libft printf
endif
