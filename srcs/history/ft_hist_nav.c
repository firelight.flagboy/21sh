/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hist_nav.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/22 16:43:13 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 11:50:21 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

static void		ft_move_to_end(int len, int *index)
{
	int		i;

	i = -1;
	while (++i < len)
		ft_move_cursor(RIGHT);
	*index = len;
}

static void		ft_hist_prev(t_hist *node, t_cursor *cr, int *index, int *len)
{
	t_listd	**head;
	int		i;

	head = get_listd();
	i = 0;
	while (node && i < cr->index_hist - 1)
	{
		i++;
		node = node->next;
	}
	if (node == NULL)
		return ;
	cr->index_hist = i;
	i = *index;
	*index = 0;
	if (*head)
		ft_listd_free(head);
	if (!(*head = ft_str_to_listd(node->cmd)))
		f_exit(0, 0, 0);
	*len = ft_listd_len_list(*head);
	while (--i >= 0)
		ft_move_cursor(LEFT);
	ft_move_to_end(*len, index);
}

static void		ft_hist_next(t_hist *node, t_cursor *cr, int *index, int *len)
{
	t_listd	**head;
	int		i;

	head = get_listd();
	i = 0;
	while (node && i < cr->index_hist + 1)
	{
		i++;
		node = node->next;
	}
	if (node == NULL)
		return ;
	cr->index_hist = i;
	i = *index;
	*index = 0;
	if (*head)
		ft_listd_free(head);
	if (!(*head = ft_str_to_listd(node->cmd)))
		f_exit(0, 0, 0);
	*len = ft_listd_len_list(*head);
	while (--i >= 0)
		ft_move_cursor(LEFT);
	ft_move_to_end(*len, index);
}

void			ft_get_history(int mode, t_cursor *cr, int *index, int *len)
{
	t_hist	**head;
	t_hist	*node;
	int		i;

	head = get_history();
	if (head == NULL || *head == NULL)
		return ;
	node = *head;
	i = *index;
	if (mode == PREV && cr->index_hist >= 0)
		ft_hist_prev(node, cr, index, len);
	else if (mode == NEXT)
		ft_hist_next(node, cr, index, len);
}
