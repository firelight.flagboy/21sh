/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_tokens_to_tab_tokens.c                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eduriez <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/06 11:24:22 by eduriez           #+#    #+#             */
/*   Updated: 2018/03/06 12:29:10 by eduriez          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lexer_parser.h"
#include "libft.h"

static t_token	ft_dup_token(const t_token token, int *dup_ok)
{
	t_token	dup_token;
	char	*value;

	value = ft_strdup(token.value);
	if (value == NULL)
	{
		*dup_ok = 0;
		dup_token.value = NULL;
		return (dup_token);
	}
	*dup_ok = 1;
	dup_token = ft_create_token(token.family, token.name, value);
	return (dup_token);
}

static void		ft_free_tab(t_token *tab)
{
	int	i;

	i = 0;
	while (tab[i].value != NULL)
	{
		ft_memdel((void**)&(tab[i].value));
		i++;
	}
	ft_memdel((void**)&tab);
}

static t_token	*ft_dup_cmd(t_token_list *begin_cmd,
							t_token *tab,
							const int size_tab)
{
	int		i;
	int		dup_ok;

	i = 0;
	dup_ok = 1;
	while (i < size_tab && dup_ok)
	{
		tab[i] = ft_dup_token(begin_cmd->token, &dup_ok);
		if (dup_ok)
		{
			i++;
			begin_cmd = begin_cmd->next;
		}
	}
	if (!dup_ok)
	{
		ft_free_tab(tab);
		return (NULL);
	}
	return (tab);
}

t_token			*ft_get_leaf_content(t_token_list *begin_cmd,
											const int size_tab)
{
	t_token *tab;

	tab = (t_token*)ft_memalloc(size_tab * sizeof(t_token));
	if (tab == NULL)
		return (NULL);
	else
		return (ft_dup_cmd(begin_cmd, tab, size_tab));
}
