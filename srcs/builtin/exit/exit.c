/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fbenneto <fbenneto@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/17 10:13:38 by fbenneto          #+#    #+#             */
/*   Updated: 2018/03/12 10:17:35 by fbenneto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_21sh.h"

int			f_exit(int argc, char **argv, t_env **environ)
{
	(void)environ;
	if (argc > 2)
		return (ft_warnx_errn(argv[0], (g_errn = ERR_ARGTOOMUCH)));
	if (*get_is_insteract())
		ft_reset_term();
	if (*get_clipboard())
		free(*get_clipboard());
	if (argc <= 1)
		exit(*get_res());
	exit(ft_atoi(argv[1]));
}
